package com.gne.pm;


import android.os.Build;

public class PM {

    static {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            System.loadLibrary("fingerPm_7");
        } else {
            System.loadLibrary("fingerPm_5");
        }

    }

    static public native int powerOn();

    static public native int powerOff();

}









































