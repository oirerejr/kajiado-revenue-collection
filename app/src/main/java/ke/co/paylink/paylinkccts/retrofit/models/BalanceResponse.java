package ke.co.paylink.paylinkccts.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BalanceResponse extends Response {

    @SerializedName("amount_balance")
    @Expose
    private String amountBalance;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("token_string")
    @Expose
    private String tokenString;
    @SerializedName("agent_number")
    @Expose
    private String agentNumber;

    public String getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(String amountBalance) {
        this.amountBalance = amountBalance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTokenString() {
        return tokenString;
    }

    public void setTokenString(String tokenString) {
        this.tokenString = tokenString;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

}