package ke.co.paylink.paylinkccts.interfaces;

import ke.co.paylink.paylinkccts.models.Bank;
import ke.co.paylink.paylinkccts.retrofit.models.RevenueType;

public interface IServiceAdapter {

    void OnServiceSelected(RevenueType type);

}
