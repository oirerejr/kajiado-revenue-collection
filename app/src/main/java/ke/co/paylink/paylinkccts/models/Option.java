package ke.co.paylink.paylinkccts.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.IdRes;
import ke.co.paylink.paylinkccts.enums.TerminalOptions;

public class Option implements Parcelable {

    public @IdRes
    int icon;
    public String action;
    public String description;
    public TerminalOptions terminalOptions;

    public Option(int icon, String action, String description, TerminalOptions terminalOptions) {
        this.icon = icon;
        this.action = action;
        this.description = description;
        this.terminalOptions = terminalOptions;
    }

    protected Option(Parcel in) {
        icon = in.readInt();
        action = in.readString();
        description = in.readString();
        terminalOptions = in.readParcelable(TerminalOptions.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(icon);
        dest.writeString(action);
        dest.writeString(description);
        dest.writeParcelable(terminalOptions, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Option> CREATOR = new Creator<Option>() {
        @Override
        public Option createFromParcel(Parcel in) {
            return new Option(in);
        }

        @Override
        public Option[] newArray(int size) {
            return new Option[size];
        }
    };
}
