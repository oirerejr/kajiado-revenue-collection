package ke.co.paylink.paylinkccts.interfaces;

import ke.co.paylink.paylinkccts.models.Bank;

public interface IBankAdapter {

    void OnBankSelected(Bank bank);

}
