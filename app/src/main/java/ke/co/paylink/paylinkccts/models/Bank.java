package ke.co.paylink.paylinkccts.models;

public class Bank {

    public String name, description;

    public Bank(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
