package ke.co.paylink.paylinkccts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.activities.AgentActivity;
import ke.co.paylink.paylinkccts.adapters.ServiceAdapter;
import ke.co.paylink.paylinkccts.databinding.ActivityServiceBinding;
import ke.co.paylink.paylinkccts.interfaces.IServiceAdapter;
import ke.co.paylink.paylinkccts.retrofit.RetrofitInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.RevenueType;
import ke.co.paylink.paylinkccts.retrofit.models.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

public class ServiceActivity extends AppCompatActivity implements IServiceAdapter {

    ActivityServiceBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_service);

        Call<Service> serviceCall = RetrofitSetup.createTransactClass(RetrofitInterface.class, "services").getServices();
        serviceCall.enqueue(new Callback<Service>() {
            @Override
            public void onResponse(Call<Service> call, Response<Service> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) return;

                    ServiceAdapter serviceAdapter = new ServiceAdapter(ServiceActivity.this, response.body().getRevenueTypes());

                    binding.services.setAdapter(serviceAdapter);

                } else {
                    ke.co.paylink.paylinkccts.retrofit.models.Response response1 = null;
                    try {
                        if (response.errorBody() != null) {
                            response1 = new Gson().fromJson(response.errorBody().string(), ke.co.paylink.paylinkccts.retrofit.models.Response.class);
                            Toast.makeText(ServiceActivity.this, response1.getStatusMessage(), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Service> call, Throwable t) {
                Toast.makeText(ServiceActivity.this, "Failed to get services", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void OnServiceSelected(RevenueType type) {
        Intent intent = new Intent();
        intent.putExtra("service", type.getId().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
