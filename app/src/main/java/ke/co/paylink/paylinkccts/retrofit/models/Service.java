package ke.co.paylink.paylinkccts.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Service {

    @SerializedName("revenueTypes")
    @Expose
    private List<RevenueType> revenueTypes = null;

    public List<RevenueType> getRevenueTypes() {
        return revenueTypes;
    }

    public void setRevenueTypes(List<RevenueType> revenueTypes) {
        this.revenueTypes = revenueTypes;
    }

}
