package ke.co.paylink.paylinkccts.models;

public class UserType {

    public String name, description;

    public UserType(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
