package ke.co.paylink.paylinkccts.retrofit;

import com.google.gson.JsonObject;

import ke.co.paylink.paylinkccts.models.User;
import ke.co.paylink.paylinkccts.retrofit.models.LoginResponse;
import ke.co.paylink.paylinkccts.retrofit.models.Service;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitInterface {

    @POST("login")
    Call<LoginResponse> getUser(@Body JsonObject body);

    @GET("revenue-types")
    Call<Service> getServices();

}
