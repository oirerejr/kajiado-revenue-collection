package ke.co.paylink.paylinkccts;

import android.app.Application;

public class Paylink extends Application {

    private static Paylink paylink;

    public static synchronized Paylink instance() {
        return paylink;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Paylink.paylink = this;
    }

}
