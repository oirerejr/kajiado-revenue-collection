package ke.co.paylink.paylinkccts.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ke.co.paylink.paylinkccts.PLSharedPrefs;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSetup {

//    private static final String BASE_URL = "https://paylink.co.ke/gt/api/index.php/";
    private static final String BASE_URL = "https://paylink.co.ke/kajiado/api/public/index.php/";

    //    private static GsonBuilder gson = new GsonBuilder().serializeNulls();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
    private static HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    public static <S> S createClass(Class<S> serviceClass, final String terminalID) {
        if (!okHttpClientBuilder.interceptors().contains(httpLoggingInterceptor)) {
            okHttpClientBuilder.addInterceptor(httpLoggingInterceptor);
            okHttpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder builder = original.newBuilder()
                            .addHeader("Terminal-ID", terminalID);
//                            .addHeader("Terminal-ID", terminalID);

                    Request request = builder.build();
                    return chain.proceed(request);
                }
            });
            builder.client(okHttpClientBuilder.build());
            retrofit = builder.build();
        }
        return retrofit.create(serviceClass);
    }

    public static <S> S createClass(Class<S> serviceClass) {
        okHttpClientBuilder.interceptors().clear();
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor);
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .addHeader("Token", PLSharedPrefs.get().getUser().getToken())
                        .addHeader("Date-Time", simpleDateFormat.format(new Date()))
                        .addHeader("Terminal-Agent", PLSharedPrefs.get().getUser().getOperatorCode())
                        .addHeader("User-ID", PLSharedPrefs.get().getUser().getUserId())
                        .addHeader("Terminal-ID", PLSharedPrefs.get().getTerminal());
//                        .addHeader("Terminal-ID", PLSharedPrefs.get().getTerminal());

                Request request = builder.build();
                return chain.proceed(request);
            }
        });
        builder.client(okHttpClientBuilder.build());
        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createTransactClass(Class<S> serviceClass, final String action) {
        okHttpClientBuilder.interceptors().clear();
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor);

        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .addHeader("Token", PLSharedPrefs.get().getUser().getToken())
                        .addHeader("Date-Time", simpleDateFormat.format(new Date()))
                        .addHeader("Terminal-Agent", PLSharedPrefs.get().getUser().getOperatorCode())
                        .addHeader("User-ID", PLSharedPrefs.get().getUser().getUserId())
                        .addHeader("Terminal-ID", PLSharedPrefs.get().getTerminal())
                        .addHeader("Action", action);
//                        .addHeader("Terminal-ID", PLSharedPrefs.get().getTerminal());

                Request request = builder.build();
                return chain.proceed(request);
            }
        });
        builder.client(okHttpClientBuilder.build());
        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }

}
