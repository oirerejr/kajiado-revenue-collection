package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivityRegistrationBinding;
import ke.co.paylink.paylinkccts.interfaces.IRegistration;
import ke.co.paylink.paylinkccts.utils.Ui;

import android.content.Intent;
import android.os.Bundle;

public class RegistrationActivity extends AppCompatActivity implements IRegistration {

    private ActivityRegistrationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registration);
        binding.setListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Ui.get().hideStatusBar(getWindow());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        Ui.get().hideStatusBar(getWindow());
    }

    @Override
    public void OnCompleteRegistration() {

    }

    @Override
    public void OnSelectService() {
        startActivity(new Intent(this, UserTypeActivity.class));
    }

}
