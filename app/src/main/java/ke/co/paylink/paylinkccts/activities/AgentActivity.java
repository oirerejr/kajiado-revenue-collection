package ke.co.paylink.paylinkccts.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.Paylink;
import ke.co.paylink.paylinkccts.PrintActivity;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.ServiceActivity;
import ke.co.paylink.paylinkccts.adapters.CardOptionsAdapter;
import ke.co.paylink.paylinkccts.databinding.ActivityAgentBinding;
import ke.co.paylink.paylinkccts.enums.TerminalOptions;
import ke.co.paylink.paylinkccts.fp.Globals;
import ke.co.paylink.paylinkccts.fragments.QRCodeFragment;
import ke.co.paylink.paylinkccts.interfaces.IAgent;
import ke.co.paylink.paylinkccts.interfaces.ICardActivity;
import ke.co.paylink.paylinkccts.interfaces.ICardOptions;
import ke.co.paylink.paylinkccts.models.Option;
import ke.co.paylink.paylinkccts.observers.OAmount;
import ke.co.paylink.paylinkccts.pos_api.POSNFCApi;
import ke.co.paylink.paylinkccts.repository.Repository;
import ke.co.paylink.paylinkccts.retrofit.CardInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.MemberResponse;
import ke.co.paylink.paylinkccts.retrofit.models.RevenueType;
import ke.co.paylink.paylinkccts.retrofit.models.Service;
import ke.co.paylink.paylinkccts.retrofit.models.TransactionResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.DBManager;
import vpos.apipackage.ByteUtil;
import vpos.apipackage.PosApiHelper;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Importer;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.digitalpersona.uareu.dpfj.ImporterImpl;
import com.gne.pm.PM;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class AgentActivity extends AppCompatActivity implements ICardActivity, ICardOptions {

    private ActivityAgentBinding binding;
    private OAmount oAmount;
    private ObservableBoolean detected = new ObservableBoolean(false);
    private boolean hasCardInformation = false;
    private CodeScanner mCodeScanner;
    private String deviceName = "";

    private int SERVICE_FETCH_INT = 45678;

    // Segregate Code Between

    byte picc_mode;
    byte picc_type = 'a';
    byte blkNo = 60;
    byte blkValue[] = new byte[20];
    byte pwd[] = new byte[20];
    byte cardtype[] = new byte[3];
    byte serialNo[] = new byte[50];
    byte dataIn[] = new byte[530];

    // End Segregate code
    int CAMERA_PERM = 100;
    private Option option;

    private Service service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent);
        binding.setListener(this);
        binding.setDetected(detected);

        Intent intent = getIntent();
        binding.setAmount(intent.getStringExtra("amount"));
        oAmount = new OAmount();
        binding.setObservers(oAmount);

        CardOptionsAdapter cardOptionsAdapter = new CardOptionsAdapter(this, Repository.instance().getCardOptionList());
        binding.cardOptionsSheet.cardOptions.setAdapter(cardOptionsAdapter);

        checkCameraPermission();

//        PM.powerOn();

        deviceName = PLSharedPrefs.get().getFPDevice();

        // Start reading the NFC
//        final POSNFCApi.IPOSAPI iposapi = new POSNFCApi.IPOSAPI() {
//            @Override
//            public void onRead(String type, String uid) {
//                request.addProperty("nfc_number", uid);
//                request.addProperty("type", type);
//                populateCard(uid, type);
//            }
//        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = -1;
                long time = System.currentTimeMillis();
                while (System.currentTimeMillis() < time + 60000) {
                    ret = readNfcCard();
                    if (ret == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestFingerprint();
                            }
                        });
                        break;
                    }
                }
            }
        }).start();

        initializeDPSKD();

    }

    @Override
    protected void onDestroy() {
        //power off
//        PM.powerOff();
        super.onDestroy();
    }

    int enableFingerprintTrial = 0;

    private void initializeDPSKD() {
        // initiliaze dp sdk
        try {
            Context applContext = getApplicationContext();
            m_reader = Globals.getInstance().getReader(deviceName, applContext);
            m_reader.Open(Reader.Priority.EXCLUSIVE);
            m_DPI = Globals.GetFirstDPI(m_reader);
            m_engine = UareUGlobal.GetEngine();
            binding.fingerStat.setText("Fingerprint enabled");
        } catch (Exception e) {
            binding.fingerStat.setText("Error during init of reader");
//            m_deviceName = "";
            onBackPressed();
            if (enableFingerprintTrial != 3) {
                initializeDPSKD();
                enableFingerprintTrial++;
            }
            return;
        }
//        try {
//            Context applContext = getApplicationContext();
//            readers = Globals.getInstance().getReaders(applContext);
//            int nSize = readers.size();
//            deviceName = (nSize == 0 ? "" : readers.get(0).GetDescription().name);
//
//            if ((deviceName != null) && !deviceName.isEmpty()) {
//                binding.fingerStat.setText("Fingerprint enabled");
//                try {
//                    m_reader = Globals.getInstance().getReader(deviceName, applContext);
//                    m_reader.Open(Reader.Priority.EXCLUSIVE);
//                    m_DPI = Globals.GetFirstDPI(m_reader);
//                } catch (Exception e) {
//                    binding.fingerStat.setText("Device: (No Reader Selected)");
//                    deviceName = "";
//                    onBackPressed();
//                    return;
//                }
//            } else {
//                binding.fingerStat.setText("Device: (No Reader Selected)");
//            }
//        } catch (UareUException e) {
//            onBackPressed();
//        }
    }

    @Override
    public void onBackPressed() {
        try {
            m_reset = true;
            try {
                m_reader.CancelCapture();
            } catch (Exception e) {
            }
            m_reader.Close();

        } catch (Exception e) {
            Log.w("UareUSampleJava", "error during reader shutdown");
        }

    }

    public void OnScanFinger(String token) {
//        synchronized (this) {
//            if (null != captureThread && !captureThread.isThreadFinished()) {
//                Toast.makeText(this, "Capture return return", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            // loop capture on a separate thread to avoid freezing the UI
//            captureThread = new CaptureThread();
//            captureThread.start();
//            binding.fingerStat.setText("Place finger on the Reader");
//        }

        synchronized (this) {
            if (null != compareThread && !compareThread.isThreadFinished()) {
                Toast.makeText(this, "Compare return return", Toast.LENGTH_SHORT).show();
                return;
            }
            compareThread = new CompareThread(token);
            compareThread.start();
            binding.fingerStat.setText("Place finger on the Reader");
        }

    }

    private void checkCameraPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // User may have declined earlier, ask Android if we should show him a reason
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Camera permission needed to read QR Code");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkCameraPermission();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();

            } else {
                // request the permission.
                // CALLBACK_NUMBER is a integer constants
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERM);
                // The callback method gets the result of the request.
            }
        } else {
            // allowed
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
//        if (hasFocus) {
//            int height = getWindow().getDecorView().getHeight() / 2;
//
//            ValueAnimator toPayAnimator = ValueAnimator.ofInt(height, 0);
//            toPayAnimator.setDuration(1000);
//            toPayAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                @Override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                    binding.topPayText.setTranslationY((int) animation.getAnimatedValue());
//                }
//            });
//            toPayAnimator.start();
//
//            int width = getWindow().getDecorView().getWidth() / 2 + 20;
//            ObjectAnimator animation = ObjectAnimator.ofFloat(binding.cardView, "translationX", (float) width);
//            animation.setDuration(1000);
//            animation.start();
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == CAMERA_PERM) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkCameraPermission();
            } else {
                // showRationale = false if user clicks Never Ask Again, otherwise true
                boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS);

                if (showRationale) {
                    // do something here to handle degraded mode
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        int width = getWindow().getDecorView().getWidth() / 2 + 20;
//        if (width > 0) {
//            ObjectAnimator animation = ObjectAnimator.ofFloat(binding.cardView, "translationX", (float) width);
//            animation.setDuration(1000);
//            animation.start();
//        }
    }

    @Override
    public void OnCardClicked() {
        //TODO
    }

    @Override
    public void OnCancelTransaction() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void ScanQRCode() {
        final QRCodeFragment qrCodeFragment = new QRCodeFragment();
        IAgent iAgent = new IAgent() {
            @Override
            public void OnResult(String result) {
                populateCard(result, "N/A");
                request.addProperty("account_number", result);
                request.addProperty("type", "NA");
                hasCardInformation = true;
                qrCodeFragment.dismiss();

                // Get fingerprint token if any
                requestFingerprint();
            }
        };

        qrCodeFragment.setiAgent(iAgent);
        qrCodeFragment.show(getSupportFragmentManager(), QRCodeFragment.class.getSimpleName());
    }

    private void requestFingerprint() {
        Call<MemberResponse> memberCall = RetrofitSetup.createClass(CardInterface.class).member(request);
        memberCall.enqueue(new Callback<MemberResponse>() {
            @Override
            public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {
                if (response.isSuccessful()) {
                    MemberResponse memberResponse = response.body();
                    if (memberResponse == null) return;
                    String token = memberResponse.getFingerprintToken();
                    OnScanFinger(token);

                } else {
                    Toast.makeText(AgentActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MemberResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void OnChangeAmount() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void OnAcceptPayment() {

    }

    @Override
    public void OnCardOptionSelected(final Option option) {
        if (option.terminalOptions == TerminalOptions.CANCEL) {
            finish();
            return;
        }

        if (!hasCardInformation) {
            Toast.makeText(this, "Scan card", Toast.LENGTH_SHORT).show();
            return;
        }

        this.option = option;

        if (option.terminalOptions == TerminalOptions.TOP_UP_CARD) {
            promptPin(option);
        } else if (option.terminalOptions == TerminalOptions.MAKE_PAYMENT) {
            startActivityForResult(new Intent(this, ServiceActivity.class), SERVICE_FETCH_INT);
        }

    }

    private AlertDialog dialog;

    private void promptPin(final Option option) {
//        currentOption = option;
        AlertDialog.Builder builder = new AlertDialog.Builder(AgentActivity.this);
        View view = LayoutInflater.from(AgentActivity.this).inflate(R.layout.layout_pin_input, null, false);
        final TextInputEditText editText = view.findViewById(R.id.card_pin);
        builder.setView(view);
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (editText.getText() != null) {
                    String pin = editText.getText().toString();
                    transact(option, pin, false);
                }
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        binding.fingerStat.setText("Fingerprint enabled");

        dialog = builder.create();
        dialog.show();
    }

    JsonObject request = new JsonObject();

    private void transact(Option option, String pin, boolean usedFingerprint) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(option.action);
        @SuppressLint("ResourceType") Drawable drawable = ContextCompat.getDrawable(this, option.icon);
        progressDialog.setIcon(drawable);
        progressDialog.setMessage(option.description);
        progressDialog.setIndeterminate(true);
//        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

        Call<TransactionResponse> transactionResponseCall = null;

        String number = binding.cardIdentifier.getText().toString();

        if (!usedFingerprint) {
            if (TextUtils.isEmpty(pin)) {
                Toast.makeText(this, "Input pin", Toast.LENGTH_SHORT).show();
                return;
            }
            request.addProperty("pin_number", pin);
            request.addProperty("auth_type", "pin");
        }

        if (TextUtils.isEmpty(number)) {
            Toast.makeText(this, "Scan Card", Toast.LENGTH_SHORT).show();
            return;
        }

        request.addProperty("subscriber_msidn", "254745254412");
        request.addProperty("amount", binding.amount.getText().toString());

//        {
//            "subscriber_msidn":"254745254412",
//                "account_number":"771617",
//                "pin_number":"1234",
//                "amount":"2000"
//        }

        switch (option.terminalOptions) {
            case TOP_UP_CARD:
                transactionResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "topup")
                        .topUp(request);
                break;
            case MAKE_PAYMENT:
                if (!request.has("renevue_type")) {
                    Toast.makeText(this, "Service not selected", Toast.LENGTH_SHORT).show();
                    return;
                }
                transactionResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "payment")
                        .payment(request);
                break;
        }

        if (transactionResponseCall == null) {
            return;
        }

        makeTransaction(transactionResponseCall, progressDialog);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && requestCode == SERVICE_FETCH_INT) {

            if (data != null) {
                String service = data.getStringExtra("service");
                request.addProperty("renevue_type", service);
//                request.addProperty("revenue_type", service);
                promptPin(Repository.instance().makePaymentOption);
            }

        }
    }

    private void makeTransaction(Call<TransactionResponse> transactionResponseCall, final ProgressDialog progressDialog) {
        transactionResponseCall.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    TransactionResponse transactionResponse = response.body();
                    if (transactionResponse != null) {
                        if (transactionResponse.getStatus().equals("200")) {

                            Intent intent1 = new Intent(AgentActivity.this, PrintActivity.class);
                            intent1.putExtra("receipt", transactionResponse.getTransactionId());
                            intent1.putExtra("amount", transactionResponse.getAmount());
                            intent1.putExtra("balance", transactionResponse.getBalance());
                            intent1.putExtra("date", transactionResponse.getDate());
                            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);
                        } else {
                            Toast.makeText(AgentActivity.this, transactionResponse.getStatusMessage(), Toast.LENGTH_LONG).show();
                        }
                        Toast.makeText(AgentActivity.this, transactionResponse.getStatusMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    ke.co.paylink.paylinkccts.retrofit.models.Response response1 = null;
                    try {
                        if (response.errorBody() != null) {
                            response1 = new Gson().fromJson(response.errorBody().string(), ke.co.paylink.paylinkccts.retrofit.models.Response.class);
                            Toast.makeText(AgentActivity.this, response1.getStatusMessage(), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AgentActivity.this, "Try again later!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void populateCard(final String card, final String type) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.cardType.setText(type);
                binding.cardIdentifier.setText(card);
                hasCardInformation = true;
            }
        });
    }

    private PosApiHelper posApiHelper = new PosApiHelper();

    public int readNfcCard() {
        byte[] NfcData_Len = new byte[5];
        byte[] Technology = new byte[25];
        byte[] NFC_UID = new byte[56];
        byte[] NDEF_message = new byte[500];

        int ret = posApiHelper.PiccNfc(NfcData_Len, Technology, NFC_UID, NDEF_message);

        int TechnologyLength = NfcData_Len[0] & 0xFF;
        int NFC_UID_length = NfcData_Len[1] & 0xFF;
        int NDEF_message_length = (NfcData_Len[3] & 0xFF) + (NfcData_Len[4] & 0xFF);
        byte[] NDEF_message_data = new byte[NDEF_message_length];
        byte[] NFC_UID_data = new byte[NFC_UID_length];
        System.arraycopy(NFC_UID, 0, NFC_UID_data, 0, NFC_UID_length);
        System.arraycopy(NDEF_message, 0, NDEF_message_data, 0, NDEF_message_length);
        String NDEF_message_data_str = new String(NDEF_message_data);
        String NDEF_str = null;
        if (!TextUtils.isEmpty(NDEF_message_data_str)) {
            NDEF_str = NDEF_message_data_str.substring(NDEF_message_data_str.indexOf("en") + 2, NDEF_message_data_str.length());
        }

        if (ret == 0) {
            posApiHelper.SysBeep();

            if (!TextUtils.isEmpty(NDEF_str)) {
                String type = new String(Technology).substring(0, TechnologyLength);
                String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
                request.addProperty("nfc_number", uid);
                request.addProperty("type", type);
                populateCard(uid, type);
            } else {
                String type = new String(Technology).substring(0, TechnologyLength);
                String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
                request.addProperty("nfc_number", uid);
                request.addProperty("type", type);
                populateCard(uid, type);
            }

        }
        return ret;
    }

    /*
   Fingerprint implementation
    */
    private BroadcastReceiver usbconn = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.hardware.usb.action.USB_DEVICE_ATTACHED".equals(action)) {
                unregisterReceiver(usbconn);
//               finish();
            }
        }
    };

    /*Compare*/
    CompareThread compareThread = null;
    boolean mCompare_bThreadFinish = false;

    StringBuffer sb;

    private DBManager mDBManager = null;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private String m_text_conclusionString;
    private Reader.CaptureResult cap_result = null;
    private byte[] bytesCapture;
    private Reader m_reader = null;
    private int m_DPI = 0;
    private Bitmap m_bitmap = null;

    public class CompareThread extends Thread {

        String data = null;

        public CompareThread(String data) {
            this.data = data;
        }

        public boolean isThreadFinished() {
            return mCompare_bThreadFinish;
        }

        @Override
        public void run() {
            super.run();
            synchronized (this) {
                mCompare_bThreadFinish = false;
                sb = new StringBuffer();
                try {
                    //Capture Finger Print 采集特征
                    cap_result = m_reader.Capture(Fid.Format.ISO_19794_4_2005, Globals.DefaultImageProcessing, m_DPI, -1);
                    Engine m_engine = UareUGlobal.GetEngine();
                    Fmd m_fmd = m_engine.CreateFmd(cap_result.image, Fmd.Format.ISO_19794_2_2005);
                    bytesCapture = m_fmd.getData();
                    Log.e("liuhao", "*******************COMPARE******************");

                    //特征比对
                    /*
                    byte1 and byte2 respectively represent the original saved fingerprint data
                    retrieved from the database or newly collected fingerprint data。
                     */
                    //byte1,byte2 是从数据库取出的原来保存的指纹数据
                    //byte[] byte1 = MyFileUtils.read(fileName);

                    //byte [] - > Fmd conversion 进行byte[] - > Fmd的转换
                    Importer importer = new ImporterImpl();
                    final Fmd fmdCapture = importer.ImportFmd(bytesCapture, Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);

                    byte[] bytesStore = Base64.decode(data, Base64.DEFAULT);
//                    byte[] bytesStore = Base64.decode(data.trim(), Base64.DEFAULT);

                    //每次new 一个 importer
                    Importer importer2 = new ImporterImpl();
                    Fmd fmdStore = importer2.ImportFmd(bytesStore, Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
//                    Fmd fmdStore = importer.ImportFmd(bytesStore, Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                    //进行比对,得到比分
                        /*
                        /*
                            *The score is between 0 and 2147483647, with 0 indicating a match and other mismatches at other times,
                            * the closer you get to 0, the more matching  you get, and at other times,
                            * the closer you get to 2147483647, the less matching
                            *分值在 0 和 2147483647 之间 , 0 表示匹配，其他不匹配
                            *越接近0，表示越匹配, 其他时，越接近2147483647，表示越不匹配
                        */

                    final int score = m_engine.Compare(fmdStore, 0, fmdCapture, 0);

                    if (score >= 0 && score <= 2147) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                request.addProperty("fingerprint_token", data);
                                transact(option, "", true);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(AgentActivity.this, "Mismatched", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    //通过Globals.GetBitmapFromRaw 转换成bitmap
//                    m_bitmap = Globals.GetBitmapFromRaw(cap_result.image.getViews()[0].getImageData(), cap_result.image.getViews()[0].getWidth(), cap_result.image.getViews()[0].getHeight());
//                    m_text_conclusionString = Globals.QualityToString(cap_result);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            int bytes = m_bitmap.getByteCount();
//                            ByteBuffer buf = ByteBuffer.allocate(bytes);
//                            m_bitmap.copyPixelsToBuffer(buf);
//                            iv_compare.setImageBitmap(m_bitmap);
//                            iv_compare.invalidate();
//                        }
//                    });

                } catch (final UareUException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            binding.fingerStat.setText(e.toString());
                        }
                    });
                    onBackPressed();
                }
                mCompare_bThreadFinish = true;
            }
        }
    }

    private final int CAPTURE = 0;
    private final int COMPARE = 1;
    CaptureThread captureThread = null;
    private boolean m_bThreadFinished = false;
    private String m_textString;
    private Engine m_engine = null;
    private boolean m_reset = false;

    int FID = 0;

    /*Capture*/
    public class CaptureThread extends Thread {

        public boolean isThreadFinished() {
            return m_bThreadFinished;
        }

        public void run() {
            super.run();
            synchronized (this) {
                m_bThreadFinished = false;

                try {
//                    while (!m_reset) {

                    /*
                     *Capture Finger Print 采集特征
                     *可根据传递的值，获取不同标准的指纹模板
                     *Different standard fingerprint templates can be obtained based on the passed values
                     */
                    cap_result = m_reader.Capture(Fid.Format.ISO_19794_4_2005, Globals.DefaultImageProcessing, m_DPI, -1);
                    Engine m_engine = UareUGlobal.GetEngine();
                    Fmd m_fmd = m_engine.CreateFmd(cap_result.image, Fmd.Format.ISO_19794_2_2005);
                    bytesCapture = m_fmd.getData();
                    Log.e("liuhao", "*******************CAPTURE******************");

                    /******************************************************/
                    /***********add by liuhao 20180522 START***************/
                    /*
                     *When you save the retrieved fingerprint ISO / ANSI bytes for database storage, you need to
                     *convert them to the basic format supported by the database. We recommend that you always  *convert them to Base64
                     *将获取到的指纹ISO / ANSI格式bytes进行数据库保存时，需要转换成数据库支持的基本格式，我们建议一定要转换成Base64
                     */
                    final String strStore = Base64.encodeToString(bytesCapture, Base64.DEFAULT);
                    //Store to database 存储到数据库

                    request.addProperty("fingerprint_token", strStore);
                    request.addProperty("fingerprint_type", String.valueOf(1));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                                transact(option, "", true);
                            }
                        }
                    });

                    FID++;

//                    setValue(FID);
                    /***********add by liuhao 20180522 END***************/
                    /******************************************************/
//                   /*
//                    Use Globals.GetBitmapFromRaw from raw to bitmap
//                    通过Globals.GetBitmapFromRaw 转换成bitmap
//                    */
//                    m_bitmap = Globals.GetBitmapFromRaw(cap_result.image.getViews()[0].getImageData(), cap_result.image.getViews()[0].getWidth(), cap_result.image.getViews()[0].getHeight());
//                    m_text_conclusionString = Globals.QualityToString(cap_result);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            binding.scanButton.setText(m_text_conclusionString);
//                        }
//                    });
                } catch (final UareUException e) {
                    if (!m_reset) {
                        Log.e("UareUSampleJava", "error during capture: " + e.toString());
                        deviceName = "";
                        onBackPressed();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AgentActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                m_bThreadFinished = true;
            }
        }
    }


}
