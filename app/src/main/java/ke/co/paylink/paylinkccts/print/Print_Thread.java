package ke.co.paylink.paylinkccts.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import com.google.zxing.BarcodeFormat;

import ke.co.paylink.paylinkccts.R;
import vpos.apipackage.PosApiHelper;
import vpos.apipackage.PrintInitException;

public class Print_Thread extends Thread {

    final int PRINT_TEST = 0;
    final int PRINT_UNICODE = 1;
    final int PRINT_BMP = 2;
    final int PRINT_BARCODE = 4;
    final int PRINT_CYCLE = 5;
    int ret = -1;

    private int voltage_level;
    private int BatteryV;
    private final static int ENABLE_RG = 10;
    private final static int DISABLE_RG = 11;

    private boolean m_bThreadFinished = true;

    private boolean is_cycle= false;
    private int cycle_num= 0;

    private int RESULT_CODE = 0;

    private String tag = this.getClass().getSimpleName();
    //private Pos pos;

    PosApiHelper posApiHelper =PosApiHelper.getInstance();

    String content = "1234567890";
    int type;
    private Context context;

    public boolean isThreadFinished() {
        return m_bThreadFinished;
    }

    public Print_Thread(Context context, int type){
        this.type = type;
        this.context = context;
    }

    public void run() {
        Log.d("Print_Thread[ run ]", "run() begin");
        Message msg1 = new Message();

        synchronized (this) {

            m_bThreadFinished = false;
            try {
                ret= posApiHelper.PrintInit();
            } catch (PrintInitException e) {
                e.printStackTrace();
                int initRet = e.getExceptionCode();
                Log.e(tag,"initRer : "+initRet);
            }

            Log.e(tag,"init code:"+ret);
            posApiHelper.PrintSetVoltage(BatteryV*2/100);

            ret = posApiHelper.PrintCheckStatus();
            if(ret == -1){
                RESULT_CODE = -1;
                Log.e(tag, "Lib_PrnCheckStatus fail, ret = " + ret);
                SendMsg("Error, No Paper ");
                m_bThreadFinished = true;
                return;
            }else if(ret == -2){
                RESULT_CODE = -1;
                Log.e(tag, "Lib_PrnCheckStatus fail, ret = " + ret);
                SendMsg("Error, Printer Too Hot ");
                m_bThreadFinished = true;
                return;
            }else if(ret == -3){
                RESULT_CODE = -1;
                Log.e(tag, "voltage = " + (BatteryV*2));
                SendMsg("Battery less "+ (BatteryV*2));
                m_bThreadFinished = true;
                return;
            }else if(voltage_level <5){
                RESULT_CODE = -1;
                Log.e(tag, "voltage_level = " + voltage_level);
                SendMsg("Battery capacity less "+ voltage_level);
                m_bThreadFinished = true;
                return;
            }else{
                RESULT_CODE = 0;
            }

            switch (type) {
                case PRINT_TEST:
                    SendMsg("PRINT_TEST");
                    posApiHelper.PrintSetFont((byte)16, (byte)16, (byte)0x33);
                    posApiHelper.PrintStr("POS签购单/POS SALES SLIP\n");
                    posApiHelper.PrintSetFont((byte)16, (byte)16, (byte)0x00);
                    posApiHelper.PrintStr("商户存根MERCHANT COPY\n");
                    posApiHelper.PrintStr("- - - - - - - - - - - - - - - - - - - - - - - -\n");
                    posApiHelper.PrintSetFont((byte)24, (byte)24, (byte)0x00);
                    posApiHelper.PrintStr("商户名称(MERCHANT NAME):\n");
                    posApiHelper.PrintStr("中国银联直连测试\n");
                    posApiHelper.PrintStr("商户编号(MERCHANT NO):\n");
                    posApiHelper.PrintStr("    001420183990573\n");
                    posApiHelper.PrintStr("终端编号(TERMINAL NO):00026715\n");
                    posApiHelper.PrintStr("操作员号(OPERATOR NO):12345678\n");
                    posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                    //	posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("发卡行(ISSUER):01020001 工商银行\n");
                    posApiHelper.PrintStr("卡号(CARD NO):\n");
                    posApiHelper.PrintStr("    9558803602109503920\n");
                    posApiHelper.PrintStr("收单行(ACQUIRER):03050011民生银行\n");
                    posApiHelper.PrintStr("交易类型(TXN. TYPE):消费/SALE\n");
                    posApiHelper.PrintStr("卡有效期(EXP. DATE):2013/08\n");
                    posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                    //	posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("批次号(BATCH NO)  :000023\n");
                    posApiHelper.PrintStr("凭证号(VOUCHER NO):000018\n");
                    posApiHelper.PrintStr("授权号(AUTH NO)   :987654\n");
                    posApiHelper.PrintStr("日期/时间(DATE/TIME):\n");
                    posApiHelper.PrintStr("    2008/01/28 16:46:32\n");
                    posApiHelper.PrintStr("交易参考号(REF. NO):200801280015\n");
                    posApiHelper.PrintStr("金额(AMOUNT):  RMB:2.55\n");
                    posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                    //	posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("备注/REFERENCE\n");
                    posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                    posApiHelper.PrintSetFont((byte)16, (byte)16, (byte)0x00);
                    posApiHelper.PrintStr("持卡人签名(CARDHOLDER SIGNATURE)\n");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("- - - - - - - - - - - - - - - - - - - - - - - -\n");
                    //	posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("  本人确认以上交易，同意将其计入本卡帐户\n");
                    posApiHelper.PrintStr("  I ACKNOWLEDGE SATISFACTORY RECEIPT\n");
                    posApiHelper.PrintStr("                                         ");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("                                         ");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("\n");

                    ret = posApiHelper.PrintStart();

                    msg1.what=ENABLE_RG;
//                    handler.sendMessage(msg1);

                    Log.d("", "Lib_PrnStart ret = " + ret);
                    if(ret != 0){
                        RESULT_CODE = -1;
                        Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                        SendMsg("Error, No Paper ");
                    }else{
                        RESULT_CODE = 0;
                    }
                    break;


                case PRINT_CYCLE:
                    SendMsg("PRINT_CYCLE");
                    posApiHelper.PrintSetFont((byte)24, (byte)24, (byte)0x00);
                    for(long dd=0;dd<100;dd++)
                    {
                        posApiHelper.PrintStr("0 1 2 3 4 5 6 7 8 9 A B C D E\n");
                    }

                    ret = posApiHelper.PrintStart();


                    msg1.what=ENABLE_RG;
//                    handler.sendMessage(msg1);

                    Log.d("", "Lib_PrnStart ret = " + ret);
                    if(ret != 0){
                        RESULT_CODE = -1;
                        Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                        SendMsg("Error, No Paper ");
                    }else{
                        RESULT_CODE = 0;
                    }
                    break;

                case PRINT_UNICODE:
                    SendMsg("PRINT_UNICODE");
                    posApiHelper.PrintSetFont((byte)24, (byte)24, (byte)0x00);

                    posApiHelper.PrintStr("中文:你好，好久不见。\n");
                    posApiHelper.PrintStr("英语:Hello, Long time no see\n");
                    posApiHelper.PrintStr("西班牙语:España, ¡Hola! Cuánto tiempo sin verte!\n");
                    // posApiHelper.PrintStr("阿拉伯语:مرحبا! وقت طويل لا رؤية!\n");
                    posApiHelper.PrintStr("法语:Bonjour! Ça fait longtemps!\n");
                    posApiHelper.PrintStr("日语:こんにちは！久しぶり！\n");
                    posApiHelper.PrintStr("俄语:Привет! Давно не виделись!\n");
                    posApiHelper.PrintStr("韩语:안녕하세요! 긴 시간은 더 볼 수 없습니다!\n");
                    String string1 = "А а, Б б, В в, Г г, Д д, Е е, Ё ё, Ж ж, З з, И и, Й й, К к, Л л, М м, Н н, О о, Ө ө, П п, Р р, С с, Т т, У у, Ү ү, Ф ф, Х х, Ц ц, Ч ч, Ш ш, Щ щ, Ъ ъ, Ь ь, Ы ы, Э э, Ю ю, Я я";
                    posApiHelper.PrintStr(string1 + "\n");
                    posApiHelper.PrintStr("                                                     \n");
                    posApiHelper.PrintStr("                                                     \n");
                    posApiHelper.PrintStr("                                                     \n");
                    posApiHelper.PrintStr("                                                     \n");

                    ret = posApiHelper.PrintStart();

                    msg1.what=ENABLE_RG;
//                    handler.sendMessage(msg1);
                    Log.d("", "Lib_PrnStart ret = " + ret);
                    if(ret != 0){
                        RESULT_CODE = -1;
                        Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                        SendMsg("Error, No Paper ");
                    }else{
                        RESULT_CODE = 0;
                    }

                    break;

                case PRINT_BMP:
                    SendMsg("PRINT_BMP");
                    Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.mipmap.metrolinx1bitdepth);
                    ret = posApiHelper.PrintBmp(bmp);
                    if(ret == 0){
                        posApiHelper.PrintStr("\n\n\n");
                        posApiHelper.PrintStr("                                         \n");
                        posApiHelper.PrintStr("                                         \n");

                        ret = posApiHelper.PrintStart();

                        msg1.what=ENABLE_RG;
//                        handler.sendMessage(msg1);

                        Log.d("", "Lib_PrnStart ret = " + ret);
                        if(ret != 0){
                            RESULT_CODE = -1;
                            Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                            SendMsg("Error, No Paper ");
                        }else{
                            RESULT_CODE = 0;
                        }
                    }else {
                        RESULT_CODE = -1;
                        SendMsg("Lib_PrnBmp Failed");
                    }
                    break;

                case PRINT_BARCODE:
                    SendMsg("PRINT_BARCODE");
                    posApiHelper.PrintBarcode(content, 360, 120, BarcodeFormat.CODE_128);
                    posApiHelper.PrintStr("CODE_128 : " + content + "\n\n");
                    posApiHelper.PrintBarcode(content, 240, 240, BarcodeFormat.QR_CODE);
                    posApiHelper.PrintStr("QR_CODE : " + content + "\n\n");
//					posApiHelper.PrintBarcode(content, 360, 120, BarcodeFormat.CODE_39);
//					posApiHelper.PrintStr("CODE_39 : " + content + "\n\n");
                    posApiHelper.PrintStr("                                        \n");
                    posApiHelper.PrintStr("                                         \n");
                    posApiHelper.PrintStr("\n");
                    posApiHelper.PrintStr("\n");

                    ret = posApiHelper.PrintStart();

                    msg1.what=ENABLE_RG;
//                    handler.sendMessage(msg1);

                    Log.d("", "Lib_PrnStart ret = " + ret);
                    if(ret != 0){
                        RESULT_CODE = -1;
                        Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                        SendMsg("Error, No Paper ");
                    }else{
                        RESULT_CODE = 0;
                    }

                    break;

                default:
                    break;
            }
            m_bThreadFinished = true;

            Log.e(tag, "goToSleep2...");
        }
    }

    public void SendMsg(String strInfo){
        Message msg = new Message();
        Bundle b = new Bundle();
        b.putString("MSG", strInfo);
        msg.setData(b);
//        handler.sendMessage(msg);
    }

}