package ke.co.paylink.paylinkccts.adapters;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.annotation.IdRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

public class BindingAdapters {

    @BindingAdapter("layout_constraintVertical_bias")
    public static void setLayoutConstraintVerticalBias(View view, float old, float bias) {
        if (old != bias) {
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) view.getLayoutParams();
            layoutParams.verticalBias = bias;
            view.setLayoutParams(layoutParams);
        }

    }

    @BindingAdapter("cardIcon")
    public static void setCardIcon(ImageView imageView, int icon) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), icon);
        imageView.setImageDrawable(drawable);
    }

    @BindingAdapter("layout_marginTop")
    public static void setLayoutMarginTop(View view, int margin) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        marginLayoutParams.topMargin = margin;
        view.setLayoutParams(marginLayoutParams);
    }

    @BindingAdapter("bottomSheetState")
    public static void setBottomSheetState(View view, int old, int state) {
        if (old != state) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view);
            bottomSheetBehavior.setState(state);
        }
    }

    @BindingAdapter("bottomSheetHidable")
    public static void setBottomSheetHidable(View view, boolean old, boolean hideable) {
        if (old != hideable) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view);
            bottomSheetBehavior.setHideable(hideable);
            bottomSheetBehavior.setFitToContents(true);
        }
    }

    @BindingAdapter("bottomSheetPeekHeight")
    public static void setBottomSheetPeekHeight(View view, int old, int height) {
        if (old != height) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view);
            bottomSheetBehavior.setPeekHeight(height);
        }
    }

    @BindingAdapter("bottomSheetFitContent")
    public static void setBottomSheetFitContent(View view, boolean old, boolean height) {
        if (old != height) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view);
            bottomSheetBehavior.setFitToContents(height);
        }
    }

    @BindingAdapter("isCurrentFocus")
    public static void setIsCurrentFocus(View view, boolean old, boolean focus) {
        if (old != focus) {
            view.setVisibility(focus ? View.VISIBLE : View.GONE);
        }
    }

    @BindingAdapter("app:isCurrentFocusAttrChanged")
    public void setListeners(View view, final InverseBindingListener bindingListener) {

    }

    @InverseBindingAdapter(attribute = "isCurrentFocus")
    public static Boolean getCurrentFocus(View view) {
        return view.getVisibility() == View.VISIBLE;
    }

}
