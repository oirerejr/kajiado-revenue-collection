package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import ke.co.paylink.paylinkccts.utils.Ui;

import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, POSDataActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Ui.get().hideStatusBar(getWindow());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            Ui.get().hideStatusBar(getWindow());
    }
}
