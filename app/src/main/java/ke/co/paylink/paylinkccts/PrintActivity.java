package ke.co.paylink.paylinkccts;

import androidx.appcompat.app.AppCompatActivity;
import ke.co.paylink.paylinkccts.activities.AgentActivity;
import ke.co.paylink.paylinkccts.activities.AmountActivity;
import vpos.apipackage.PosApiHelper;
import vpos.apipackage.PrintInitException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import java.util.Timer;

public class PrintActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);

        textViewMsg = findViewById(R.id.desc);

        init_Gray();
        setValue(3);

        if (printThread != null && !printThread.isThreadFinished()) {
            Log.e(tag, "Thread is still running...");
            return;
        }

        Intent intent = getIntent();
        String receipt = intent.getStringExtra("receipt");
        String amount = intent.getStringExtra("amount");
        String balance = intent.getStringExtra("balance");
        String date = intent.getStringExtra("date");

        printThread = new Print_Thread(PRINT_UNICODE, receipt, amount, balance, date);
        printThread.start();

    }

    @Override
    public void onBackPressed() {
        if (printThread != null && !printThread.isThreadFinished()) {
            Log.e(tag, "Thread is still running...");
            return;
        }
        onClickQuit();
    }

    public String tag = "PrintActivity";

    final int PRINT_TEST = 0;
    final int PRINT_UNICODE = 1;
    final int PRINT_BMP = 2;
    final int PRINT_BARCODE = 4;
    final int PRINT_CYCLE = 5;

    private RadioGroup rg = null;
    private Timer timer;
    private Timer timer2;
    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private int voltage_level;
    private int BatteryV;
    SharedPreferences preferences;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private final static int ENABLE_RG = 10;
    private final static int DISABLE_RG = 11;

    TextView textViewMsg = null;
    TextView textViewGray = null;
    int ret = -1;
    private boolean m_bThreadFinished = true;

    private boolean is_cycle = false;
    private int cycle_num = 0;

    private int RESULT_CODE = 0;
    private static final String DISABLE_FUNCTION_LAUNCH_ACTION = "android.intent.action.DISABLE_FUNCTION_LAUNCH";
    //private Pos pos;

    PosApiHelper posApiHelper = PosApiHelper.getInstance();

    private void setValue(int val) {
        sp = getSharedPreferences("Gray", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("value", val);
        editor.commit();
    }

    private int getValue() {
        sp = getSharedPreferences("Gray", MODE_PRIVATE);
        int value = sp.getInt("value", 2);
        return value;
    }

    private void init_Gray() {
        int flag = getValue();
        posApiHelper.PrintSetGray(flag);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        disableFunctionLaunch(true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onResume();
        filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        receiver = new BatteryReceiver();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        disableFunctionLaunch(false);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onPause();
        QuitHandler();
        unregisterReceiver(receiver);
    }

    public void onClickQuit() {

        if (timer != null) {
            timer.cancel();
            // 一定设置为null，否则定时器不会被回收
            timer = null;
        }

        if (timer2 != null) {
            timer.cancel();
            // 一定设置为null，否则定时器不会被回收
            timer2 = null;
        }
        //  wakeLock.release();

        //if(m_bThreadFinished)
        QuitHandler();
        Intent intent1 = new Intent(this, AmountActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent1);
    }

//
//    public void onClickUnicodeTest(View v) {
//        if (printThread != null && !printThread.isThreadFinished()) {
//            Log.e(tag, "Thread is still running...");
//            return;
//        }
//
//        printThread = new Print_Thread(PRINT_UNICODE);
//        printThread.start();
//
//    }
//
//    public void OnClickBarcode(View view) {
//        if (printThread != null && !printThread.isThreadFinished()) {
//            Log.e(tag, "Thread is still running...");
//            return;
//        }
//
//
//        printThread = new Print_Thread(PRINT_BARCODE);
//        printThread.start();
//    }
//
//    public void onClickBmp(View view) {
//        if (printThread != null && !printThread.isThreadFinished()) {
//            Log.e(tag, "Thread is still running...");
//            return;
//        }
//
//        printThread = new Print_Thread(PRINT_BMP);
//        printThread.start();
//
//    }
//
//
//    public void onClickCycle(View v) {
//        if (printThread != null && !printThread.isThreadFinished()) {
//            Log.e(tag, "Thread is still running...");
//            return;
//        }
//
//        if (is_cycle == false) {
//            is_cycle = true;
//            preferences = getSharedPreferences("count", MODE_WORLD_READABLE);
//            cycle_num = preferences.getInt("count", 0);
//            SendMsg("total cycle num =" + cycle_num);
//
//            handlers.postDelayed(runnable, 3000);
//
//        }
//    }
//
//    public void onClickClean(View v) {
//        if (printThread != null && !printThread.isThreadFinished()) {
//            Log.e(tag, "Thread is still running...");
//            return;
//        }
//        textViewMsg.setText("");
//        preferences = getSharedPreferences("count", MODE_WORLD_READABLE);
//        cycle_num = preferences.getInt("count", 0);
//        editor = preferences.edit();
//        cycle_num = 0;
//        editor.putInt("count", cycle_num);
//        editor.commit();
//        QuitHandler();
//
//    }

    public void QuitHandler() {
        is_cycle = false;
//        handlers.removeCallbacks(runnable);
    }


//    Handler handlers = new Handler();
//    Runnable runnable = new Runnable() {
//
//        @Override
//        public void run() {
//            // TODO Auto-generated method stub
//
//            Log.e(tag, "TIMER log...");
//            printThread = new Print_Thread(PRINT_UNICODE);
//            printThread.start();
//
//            Log.e(tag, "TIMER log2...");
//            if (RESULT_CODE == 0) {
//                editor = preferences.edit();
//                editor.putInt("count", ++cycle_num);
//                editor.commit();
//                Log.e(tag, "cycle num=" + cycle_num);
//                SendMsg("cycle num =" + cycle_num);
//            }
//            handlers.postDelayed(this, 9000);
//
//        }
//    };

    Print_Thread printThread = null;

    public class Print_Thread extends Thread {

        String content = "1234567890";
        int type;
        String receipt, amount, balance, date;

        public boolean isThreadFinished() {
            return m_bThreadFinished;
        }

        public Print_Thread(int type, String receipt, String amount, String balance, String date) {
            this.type = type;
            this.amount = amount;
            this.balance = balance;
            this.date = date;
            this.receipt = receipt;
        }

        public void run() {
            Log.d("Print_Thread[ run ]", "run() begin");
            Message msg1 = new Message();

            synchronized (this) {

                m_bThreadFinished = false;
                try {
                    ret = posApiHelper.PrintInit();
                } catch (PrintInitException e) {
                    e.printStackTrace();
                    int initRet = e.getExceptionCode();
                    Log.e(tag, "initRer : " + initRet);
                }

                Log.e(tag, "init code:" + ret);
                posApiHelper.PrintSetVoltage(BatteryV * 2 / 100);

                ret = posApiHelper.PrintCheckStatus();
                if (ret == -1) {
                    RESULT_CODE = -1;
                    Log.e(tag, "Lib_PrnCheckStatus fail, ret = " + ret);
                    SendMsg("Error, No Paper ");
                    m_bThreadFinished = true;
                    return;
                } else if (ret == -2) {
                    RESULT_CODE = -1;
                    Log.e(tag, "Lib_PrnCheckStatus fail, ret = " + ret);
                    SendMsg("Error, Printer Too Hot ");
                    m_bThreadFinished = true;
                    return;
                } else if (ret == -3) {
                    RESULT_CODE = -1;
                    Log.e(tag, "voltage = " + (BatteryV * 2));
                    SendMsg("Battery less " + (BatteryV * 2));
                    m_bThreadFinished = true;
                    return;
                } else if (voltage_level < 5) {
                    RESULT_CODE = -1;
                    Log.e(tag, "voltage_level = " + voltage_level);
                    SendMsg("Battery capacity less " + voltage_level);
                    m_bThreadFinished = true;
                    return;
                } else {
                    RESULT_CODE = 0;
                }

                switch (type) {
                    case PRINT_TEST:
                        SendMsg("PRINT_TEST");
                        posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x33);
                        posApiHelper.PrintStr("POS签购单/POS SALES SLIP\n");
                        posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x00);
                        posApiHelper.PrintStr("商户存根MERCHANT COPY\n");
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - - - - - - - - - -\n");
                        posApiHelper.PrintSetFont((byte) 24, (byte) 24, (byte) 0x00);
                        posApiHelper.PrintStr("商户名称(MERCHANT NAME):\n");
                        posApiHelper.PrintStr("中国银联直连测试\n");
                        posApiHelper.PrintStr("商户编号(MERCHANT NO):\n");
                        posApiHelper.PrintStr("    001420183990573\n");
                        posApiHelper.PrintStr("终端编号(TERMINAL NO):00026715\n");
                        posApiHelper.PrintStr("操作员号(OPERATOR NO):12345678\n");
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                        //	posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("发卡行(ISSUER):01020001 工商银行\n");
                        posApiHelper.PrintStr("卡号(CARD NO):\n");
                        posApiHelper.PrintStr("    9558803602109503920\n");
                        posApiHelper.PrintStr("收单行(ACQUIRER):03050011民生银行\n");
                        posApiHelper.PrintStr("交易类型(TXN. TYPE):消费/SALE\n");
                        posApiHelper.PrintStr("卡有效期(EXP. DATE):2013/08\n");
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                        //	posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("批次号(BATCH NO)  :000023\n");
                        posApiHelper.PrintStr("凭证号(VOUCHER NO):000018\n");
                        posApiHelper.PrintStr("授权号(AUTH NO)   :987654\n");
                        posApiHelper.PrintStr("日期/时间(DATE/TIME):\n");
                        posApiHelper.PrintStr("    2008/01/28 16:46:32\n");
                        posApiHelper.PrintStr("交易参考号(REF. NO):200801280015\n");
                        posApiHelper.PrintStr("金额(AMOUNT):  RMB:2.55\n");
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                        //	posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("备注/REFERENCE\n");
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                        posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x00);
                        posApiHelper.PrintStr("持卡人签名(CARDHOLDER SIGNATURE)\n");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - - - - - - - - - -\n");
                        //	posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("  本人确认以上交易，同意将其计入本卡帐户\n");
                        posApiHelper.PrintStr("  I ACKNOWLEDGE SATISFACTORY RECEIPT\n");
                        posApiHelper.PrintStr("                                         ");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("                                         ");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("\n");

                        ret = posApiHelper.PrintStart();

                        msg1.what = ENABLE_RG;
                        handler.sendMessage(msg1);

                        Log.d("", "Lib_PrnStart ret = " + ret);
                        if (ret != 0) {
                            RESULT_CODE = -1;
                            Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                            SendMsg("Error, No Paper ");
                        } else {
                            RESULT_CODE = 0;
                        }
                        break;


                    case PRINT_CYCLE:
                        SendMsg("PRINT_CYCLE");
                        posApiHelper.PrintSetFont((byte) 24, (byte) 24, (byte) 0x00);
                        for (long dd = 0; dd < 100; dd++) {
                            posApiHelper.PrintStr("0 1 2 3 4 5 6 7 8 9 A B C D E\n");
                        }

                        ret = posApiHelper.PrintStart();


                        msg1.what = ENABLE_RG;
                        handler.sendMessage(msg1);

                        Log.d("", "Lib_PrnStart ret = " + ret);
                        if (ret != 0) {
                            RESULT_CODE = -1;
                            Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                            SendMsg("Error, No Paper ");
                        } else {
                            RESULT_CODE = 0;
                        }
                        break;

                    case PRINT_UNICODE:
                        posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x33);

                        posApiHelper.PrintStr("POS SALES SLIP\n");

                        posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x00);
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - - - - - - - - - -\n");

                        posApiHelper.PrintSetFont((byte) 24, (byte) 24, (byte) 0x00);

                        posApiHelper.PrintStr("(MERCHANT NAME): \n");
                        posApiHelper.PrintStr(String.format("%s\n", PLSharedPrefs.get().getUser().getNames()));
                        posApiHelper.PrintStr("(MERCHANT NO):\n");
                        posApiHelper.PrintStr(String.format("%s:\n", PLSharedPrefs.get().getUser().getOperatorCode()));
                        posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
                        posApiHelper.PrintStr("(Payment Channel) : Card\n");
                        posApiHelper.PrintStr(String.format("(Receipt Number) : %s\n", receipt));
                        posApiHelper.PrintStr(String.format("(Amount Paid) : %s\n", amount));
                        posApiHelper.PrintStr(String.format("(Balance) : %s\n", balance));
                        posApiHelper.PrintStr(String.format("(Date) : %s\n", date));
                        posApiHelper.PrintStr("                                                     \n");
                        posApiHelper.PrintStr("                                                     \n");
                        posApiHelper.PrintStr("                                                     \n");
                        posApiHelper.PrintStr("                                                     \n");


                        ret = posApiHelper.PrintStart();

                        msg1.what = ENABLE_RG;
                        handler.sendMessage(msg1);
                        Log.d("", "Lib_PrnStart ret = " + ret);
                        if (ret != 0) {
                            RESULT_CODE = -1;
                            Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                            SendMsg("Error, No Paper ");
                        } else {
                            RESULT_CODE = 0;
                        }

                        onClickQuit();

                        break;

                    case PRINT_BMP:
                        SendMsg("PRINT_BMP");
                        Bitmap bmp = BitmapFactory.decodeResource(PrintActivity.this.getResources(), R.mipmap.metrolinx1bitdepth);
                        ret = posApiHelper.PrintBmp(bmp);
                        if (ret == 0) {
                            posApiHelper.PrintStr("\n\n\n");
                            posApiHelper.PrintStr("                                         \n");
                            posApiHelper.PrintStr("                                         \n");

                            ret = posApiHelper.PrintStart();

                            msg1.what = ENABLE_RG;
                            handler.sendMessage(msg1);

                            Log.d("", "Lib_PrnStart ret = " + ret);
                            if (ret != 0) {
                                RESULT_CODE = -1;
                                Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                                SendMsg("Error, No Paper ");
                            } else {
                                RESULT_CODE = 0;
                            }
                        } else {
                            RESULT_CODE = -1;
                            SendMsg("Lib_PrnBmp Failed");
                        }
                        break;

                    case PRINT_BARCODE:
                        SendMsg("PRINT_BARCODE");
                        posApiHelper.PrintBarcode(content, 360, 120, BarcodeFormat.CODE_128);
                        posApiHelper.PrintStr("CODE_128 : " + content + "\n\n");
                        posApiHelper.PrintBarcode(content, 240, 240, BarcodeFormat.QR_CODE);
                        posApiHelper.PrintStr("QR_CODE : " + content + "\n\n");
//					posApiHelper.PrintBarcode(content, 360, 120, BarcodeFormat.CODE_39);
//					posApiHelper.PrintStr("CODE_39 : " + content + "\n\n");
                        posApiHelper.PrintStr("                                        \n");
                        posApiHelper.PrintStr("                                         \n");
                        posApiHelper.PrintStr("\n");
                        posApiHelper.PrintStr("\n");

                        ret = posApiHelper.PrintStart();

                        msg1.what = ENABLE_RG;
                        handler.sendMessage(msg1);

                        Log.d("", "Lib_PrnStart ret = " + ret);
                        if (ret != 0) {
                            RESULT_CODE = -1;
                            Log.e(tag, "Lib_PrnStart fail, ret = " + ret);
                            SendMsg("Error, No Paper ");
                        } else {
                            RESULT_CODE = 0;
                        }

                        break;

                    default:
                        break;
                }
                m_bThreadFinished = true;

                Log.e(tag, "goToSleep2...");
            }
        }
    }

    /**
     * todo
     * to test cs10 print
     */
    public void onClickTestCs10Print(View v) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                toTestCs10Print();
            }
        }).start();

    }


    private int toTestCs10Print() {
        PosApiHelper _posApiHelper = PosApiHelper.getInstance();

        synchronized (this) {
            int res = 0;
            try {
                res = _posApiHelper.PrintInit();
            } catch (PrintInitException e) {
                e.printStackTrace();
                int initRet = e.getExceptionCode();
                Log.e(tag, "initRer : " + initRet);
            }
            Log.e(tag, "PrintInit -> " + res);

            if (res == 0) {
                _posApiHelper.PrintSetVoltage(75);
            } else {
                Log.e(tag, "init failed ~ " + res);
                return res;
            }

            res = _posApiHelper.PrintCheckStatus();
            if (res == 0) {
                _posApiHelper.PrintSetGray(2);
            } else {
                Log.e(tag, "CheckStatus failed  ~ " + res);
                return res;
            }

            _posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x33);
            _posApiHelper.PrintStr("Selpal Sales Slip\n");
            _posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x00);
            _posApiHelper.PrintStr("MERCHANT COPY\n");
            _posApiHelper.PrintStr("- - - - - - - - - - - - - - - - - - - - - - - -\n");
            _posApiHelper.PrintSetFont((byte) 24, (byte) 24, (byte) 0x00);
            _posApiHelper.PrintStr("(MERCHANT NAME):\n");
            _posApiHelper.PrintStr("Trevor Spaza\n");
            _posApiHelper.PrintStr("(MERCHANT NO): tre0242\n");
            _posApiHelper.PrintStr("(TERMINAL NO):00026715\n");
            _posApiHelper.PrintStr("(OPERATOR NO):12345678\n");
            _posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
            _posApiHelper.PrintStr("(ISSUER):01020001\n");
            _posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
            _posApiHelper.PrintStr("(AMOUNT):  RMB:2.55\n");
            _posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
            _posApiHelper.PrintSetFont((byte) 16, (byte) 16, (byte) 0x00);
            _posApiHelper.PrintStr("  I ACKNOWLEDGE SATISFACTORY RECEIPT\n");
            _posApiHelper.PrintStr("- - - - - - - - - - - - - - - -\n");
            _posApiHelper.PrintStr("\n");
            _posApiHelper.PrintStr("                                         ");
            _posApiHelper.PrintStr("\n");
            _posApiHelper.PrintStr("\n");
            _posApiHelper.PrintStr("\n");

            res = _posApiHelper.PrintStart();
            if (res == 0) {
                Log.e(tag, "PrintStart success ~" + res);
                return res;
            } else {
                Log.e(tag, "PrintStart Failed ~" + res);
                return res;
            }
        }

    }


    public void SendMsg(String strInfo) {
        Message msg = new Message();
        Bundle b = new Bundle();
        b.putString("MSG", strInfo);
        msg.setData(b);
        handler.sendMessage(msg);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case DISABLE_RG:
                    break;

                case ENABLE_RG:

                    break;
                default:
                    Bundle b = msg.getData();
                    String strInfo = b.getString("MSG");
                    textViewMsg.setText(strInfo);
                    Log.d("Msr", strInfo);

                    break;
            }
        }
    };

    public class BatteryReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            voltage_level = intent.getExtras().getInt("level");// ��õ�ǰ����
            Log.e("wbw", "current  = " + voltage_level);
            BatteryV = intent.getIntExtra("voltage", 0);  //电池电压
            Log.e("wbw", "BatteryV  = " + BatteryV);
            Log.e("wbw", "V  = " + BatteryV * 2 / 100);
            //	m_voltage = (int) (65+19*voltage_level/100); //放大十倍
            //   Log.e("wbw","m_voltage  = " + m_voltage );
        }
    }

    // disable the power key when the device is boot from alarm but not ipo boot
    private void disableFunctionLaunch(boolean state) {
        Intent disablePowerKeyIntent = new Intent(DISABLE_FUNCTION_LAUNCH_ACTION);
        if (state) {
            disablePowerKeyIntent.putExtra("state", true);
        } else {
            disablePowerKeyIntent.putExtra("state", false);
        }
        sendBroadcast(disablePowerKeyIntent);
    }

    @SuppressWarnings("static-access")
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("onKeyDown", "keyCode = " + keyCode);
        return super.onKeyDown(keyCode, event);
    }

}
