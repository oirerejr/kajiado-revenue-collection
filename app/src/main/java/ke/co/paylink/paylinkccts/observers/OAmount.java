package ke.co.paylink.paylinkccts.observers;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

public class OAmount extends BaseObservable {

    public ObservableBoolean editMode = new ObservableBoolean(true);
    public ObservableBoolean roamMode = new ObservableBoolean(false);
    public final ObservableField<String> amountText = new ObservableField<>("Transaction amount");
    public final ObservableInt cardOptionBottomSheetState = new ObservableInt(BottomSheetBehavior.STATE_EXPANDED);
    //    public final ObservableInt cardOptionBottomSheetHeight = new ObservableInt(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
    public final ObservableBoolean cardOptionBottomSheetHidable = new ObservableBoolean(false);
    public final ObservableBoolean cardOptionBottomSheetFitContent = new ObservableBoolean(true);
    public final ObservableBoolean cardMode = new ObservableBoolean(false);
    public final ObservableBoolean showNext = new ObservableBoolean(false);

    public final ObservableInt agentOptionBottomSheetState = new ObservableInt(BottomSheetBehavior.STATE_EXPANDED);
//    public final ObservableInt agentOptionBottomSheetHeight = new ObservableInt(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
    public final ObservableBoolean agentOptionBottomSheetHidable = new ObservableBoolean(false);
    public final ObservableBoolean agentOptionBottomSheetFitContent = new ObservableBoolean(true);

    @Bindable
    public ObservableBoolean getRoamMode() {
        return roamMode;
    }

    public void setRoamMode(ObservableBoolean roamMode) {
        this.roamMode = roamMode;
        editMode.set(!roamMode.get());
    }

}
