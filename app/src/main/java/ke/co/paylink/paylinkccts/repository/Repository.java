package ke.co.paylink.paylinkccts.repository;

import java.util.Arrays;
import java.util.List;

import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.enums.TerminalOptions;
import ke.co.paylink.paylinkccts.models.Option;

public class Repository {

    private static Repository repository;

    private final Option refundOption = new Option(R.drawable.ic_refund, "Reverse", "Undo a previous transaction", TerminalOptions.REFUND);
    private final Option registerOption = new Option(R.drawable.ic_credit_card, "Register Customer", "Register a new user", TerminalOptions.REGISTER);
    private final Option checkBalanceOption = new Option(R.drawable.ic_wallet, "Check Balance", "Check amount in a wallet", TerminalOptions.CHECK_BALANCE);
    private final Option endSessionOption = new Option(R.drawable.ic_power_button, "End Session", "Terminate current session", TerminalOptions.END_SESSION);
    private final List<Option> agentOption = Arrays.asList(registerOption, refundOption, checkBalanceOption, endSessionOption);

    public final Option makePaymentOption = new Option(R.drawable.ic_make_payment, "Make Payment", "Service charge", TerminalOptions.MAKE_PAYMENT);
    private final Option topUpOption = new Option(R.drawable.ic_top_up, "Top Up Card", "Transfer to my card", TerminalOptions.TOP_UP_CARD);
    private final Option cancelOption = new Option(R.drawable.ic_cancel_2, "Cancel Transaction", "Cancel this transaction", TerminalOptions.CANCEL);
    //    private final Option withdrawOption = new Option(R.drawable.ic_transfer, "Withdraw", "Receive Cash", TerminalOptions.WITHDRAW);
    private final List<Option> cardOptionList = Arrays.asList(makePaymentOption, topUpOption, cancelOption);

    public static synchronized Repository instance() {
        if (repository == null) {
            repository = new Repository();
        }
        return repository;
    }

    public List<Option> getAgentOption() {
        return agentOption;
    }

    public List<Option> getCardOptionList() {
        return cardOptionList;
    }

}
