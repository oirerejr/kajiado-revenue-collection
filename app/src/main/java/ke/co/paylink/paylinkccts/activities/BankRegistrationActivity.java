package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.adapters.BankAdapter;
import ke.co.paylink.paylinkccts.databinding.ActivityBankRegistrationBinding;
import ke.co.paylink.paylinkccts.interfaces.IBankAdapter;
import ke.co.paylink.paylinkccts.models.Bank;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BankRegistrationActivity extends AppCompatActivity implements IBankAdapter {

    private Boolean editMode;
    ActivityBankRegistrationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bank_registration);
        binding.setEditMode(false);

        binding.bankName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                binding.setEditMode(hasFocus);
            }
        });

        Bank bank = new Bank("Standard Chartered", "Dar, Tanzania");
        Bank bank1 = new Bank("IBM Bank", "Dar, Tanzania");
        Bank bank2 = new Bank("Cooperative Bank", "Dar, Tanzania");

        List<Bank> banks = Arrays.asList(bank, bank1, bank2);
        BankAdapter bankAdapter = new BankAdapter(this, banks);
        binding.banks.setAdapter(bankAdapter);

    }

    @Override
    public void onBackPressed() {
        View view = getCurrentFocus();
        if (view != null) {
            view.clearFocus();
//            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    @Override
    public void OnBankSelected(Bank bank) {
        
    }
}
