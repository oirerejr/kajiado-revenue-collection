package ke.co.paylink.paylinkccts.view_models;

import android.content.Context;
import android.location.Location;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import ke.co.paylink.paylinkccts.live_data.LocationLiveData;

public class LocationViewModel extends ViewModel {

    private LocationLiveData locationLiveData;

    public LocationLiveData getModel(Context context) {
        if (locationLiveData == null) {
            locationLiveData = new LocationLiveData(context);
        }
        return locationLiveData;
    }

}
