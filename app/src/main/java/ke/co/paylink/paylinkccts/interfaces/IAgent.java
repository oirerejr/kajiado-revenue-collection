package ke.co.paylink.paylinkccts.interfaces;

public interface IAgent {
    void OnResult(String result);
}
