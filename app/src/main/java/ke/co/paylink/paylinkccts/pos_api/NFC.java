//package ke.co.paylink.paylinkccts.pos_api;
//
//import android.text.TextUtils;
//import android.util.Log;
//import android.widget.Toast;
//
//import vpos.apipackage.ByteUtil;
//import vpos.apipackage.PosApiHelper;
//
//public class NFC {
//
//    private static PosApiHelper posApiHelper = new PosApiHelper();
//
//    byte picc_mode;
//    byte picc_type = 'a';
//    byte blkNo = 60;
//    byte blkValue[] = new byte[20];
//    byte pwd[] = new byte[20];
//    byte cardtype[] = new byte[3];
//    byte serialNo[] = new byte[50];
//    byte dataIn[] = new byte[530];
//
//    public static int readNfcCard() {
//
//        byte[] NfcData_Len = new byte[5];
//        byte[] Technology = new byte[25];
//        byte[] NFC_UID = new byte[56];
//        byte[] NDEF_message = new byte[500];
//
//        int ret = posApiHelper.PiccNfc(NfcData_Len, Technology, NFC_UID, NDEF_message);
//
//        int TechnologyLength = NfcData_Len[0] & 0xFF;
//        int NFC_UID_length = NfcData_Len[1] & 0xFF;
//        int NDEF_message_length = (NfcData_Len[3] & 0xFF) + (NfcData_Len[4] & 0xFF);
//        byte[] NDEF_message_data = new byte[NDEF_message_length];
//        byte[] NFC_UID_data = new byte[NFC_UID_length];
//        System.arraycopy(NFC_UID, 0, NFC_UID_data, 0, NFC_UID_length);
//        System.arraycopy(NDEF_message, 0, NDEF_message_data, 0, NDEF_message_length);
//        String NDEF_message_data_str = new String(NDEF_message_data);
//        String NDEF_str = null;
//        if (!TextUtils.isEmpty(NDEF_message_data_str)) {
//            NDEF_str = NDEF_message_data_str.substring(NDEF_message_data_str.indexOf("en") + 2, NDEF_message_data_str.length());
//        }
//
//        if (ret == 0) {
//            posApiHelper.SysBeep();
//
//            if (!TextUtils.isEmpty(NDEF_str)) {
//                String type = new String(Technology).substring(0, TechnologyLength);
//                String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
////                populateCard(uid);
//            } else {
//                String type = new String(Technology).substring(0, TechnologyLength);
//                String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
////                populateCard(uid);
//            }
//
//        }
//        return ret;
//    }
//
//}
