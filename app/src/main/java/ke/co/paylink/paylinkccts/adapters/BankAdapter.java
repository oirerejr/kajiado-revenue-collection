package ke.co.paylink.paylinkccts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ke.co.paylink.paylinkccts.databinding.ItemBankBinding;
import ke.co.paylink.paylinkccts.interfaces.IBankAdapter;
import ke.co.paylink.paylinkccts.models.Bank;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder> {

    private Context context;
    private List<Bank> banks;
    private IBankAdapter iBankAdapter;

    public BankAdapter(Context context, List<Bank> banks) {
        this.context = context;
        this.banks = banks;
        iBankAdapter = (IBankAdapter) context;
    }

    @NonNull
    @Override
    public BankAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBankBinding bankBinding = ItemBankBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(bankBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BankAdapter.ViewHolder holder, int position) {
        Bank bank = banks.get(position);
        holder.bind(bank);
    }

    @Override
    public int getItemCount() {
        return banks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemBankBinding binding;

        public ViewHolder(@NonNull ItemBankBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Bank bank) {
            binding.setBank(bank);
            binding.setListener(iBankAdapter);
            binding.executePendingBindings();
        }

    }

}
