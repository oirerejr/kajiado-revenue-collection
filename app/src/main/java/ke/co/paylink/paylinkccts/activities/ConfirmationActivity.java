package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivityConfirmationBinding;
import ke.co.paylink.paylinkccts.interfaces.IConfirmationActivity;
import ke.co.paylink.paylinkccts.models.Option;
import ke.co.paylink.paylinkccts.retrofit.CardInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.TransactionResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.JsonObject;

public class ConfirmationActivity extends AppCompatActivity {

    private ActivityConfirmationBinding binding;
//    private Call<TransactionResponse> transactionResponseCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirmation);
//        binding.setListener(this);

        Intent intent = getIntent();

        String amount = intent.getStringExtra("amount");
        String cardNumber = intent.getStringExtra("card_number");
        Option option = intent.getParcelableExtra("card");

        binding.setAction(option.action);
        binding.setAmount(amount);
        binding.setIcon(option.icon);

        JsonObject request = new JsonObject();
        request.addProperty("card_number", cardNumber);
        request.addProperty("amount", amount);

//        switch (option.terminalOptions) {
//            case WITHDRAW:
//                transactionResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "withdraw")
//                        .transact(request);
//                break;
//            case TOP_UP_CARD:
//                transactionResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "topup")
//                        .transact(request);
//                break;
//            case MAKE_PAYMENT:
//                transactionResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "payment")
//                        .transact(request);
//                break;
//            case TRANSFER_TO_OTHER_CARD:
//                break;
//        }

//        makeTransaction();

    }

//    private void makeTransaction() {
//        transactionResponseCall.enqueue(new Callback<TransactionResponse>() {
//            @Override
//            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
//                if (response.isSuccessful()) {
//                    TransactionResponse transactionResponse = response.body();
//                    if (transactionResponse != null) {
//                        if (transactionResponse.getStatus().equals("200")) {
//                            Intent intent1 = new Intent(ConfirmationActivity.this, AmountActivity.class);
//                            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(intent1);
//                        } else {
//                            error(transactionResponse.getStatusMessage());
//                        }
//                        Toast.makeText(ConfirmationActivity.this, transactionResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
//                    } else {
//                        error("Unauthorized");
//                    }
//                } else {
//                    error("Request unsuccessful, try again later");
////                    binding.statusText.setText(transactionResponse.getStatusMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<TransactionResponse> call, Throwable t) {
//                error(t.getMessage());
//            }
//        });
//    }

//    private void error(String message) {
//        ((Activity) this).setTheme(R.style.AppTheme_Error);
//        binding.statusText.setText(message);
//        binding.parent.setBackgroundColor(ContextCompat.getColor(ConfirmationActivity.this, R.color.colorRedAccent));
//        binding.action.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.white));
//        binding.statusText.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.white));
//        binding.amount.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.white));
//        binding.currencyText.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.white));
//        Drawable whiteBackground = ContextCompat.getDrawable(this, R.drawable.button_background_white);
////        Drawable blueBackground = ContextCompat.getDrawable(this, R.drawable.accept_payment_background);
//        binding.cancelTransaction.setBackground(whiteBackground);
//        binding.cancelTransaction.setText("Resend");
//        binding.cancelTransaction.setTextColor(ContextCompat.getColor(this, R.color.colorRedAccent));
//
//        transactionResponseCall = transactionResponseCall.clone();
//    }
//
//    private void normal(String message) {
//        binding.statusText.setText(message);
//
//        ((Activity) this).setTheme(R.style.AppTheme_White);
//        binding.parent.setBackgroundColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.white));
//        binding.action.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.black));
//        binding.statusText.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, R.color.colorGrayAccent));
//        binding.amount.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.black));
//        binding.currencyText.setTextColor(ContextCompat.getColor(ConfirmationActivity.this, android.R.color.black));
//
////        Drawable whiteBackground = ContextCompat.getDrawable(this, R.drawable.button_background_white);
//        Drawable blueBackground = ContextCompat.getDrawable(this, R.drawable.accept_payment_background);
//        binding.cancelTransaction.setBackground(blueBackground);
//        binding.cancelTransaction.setText("Cancel Transaction");
//        binding.cancelTransaction.setTextColor(ContextCompat.getColor(this, android.R.color.white));
//    }
//
//    @Override
//    public void OnCancelTransaction() {
//        if (transactionResponseCall != null) {
//
//            if (transactionResponseCall.isExecuted()) {
//                transactionResponseCall.cancel();
//                finish();
//            } else {
//                normal("Waiting for confirmation");
//                makeTransaction();
//            }
//
//        }
//    }
}
