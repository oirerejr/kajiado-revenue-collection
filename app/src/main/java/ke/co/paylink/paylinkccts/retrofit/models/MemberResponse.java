package ke.co.paylink.paylinkccts.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberResponse {

    @SerializedName("fingerprint_token")
    @Expose
    private String fingerprintToken;
    @SerializedName("subscriber_msidn")
    @Expose
    private String subscriberMsidn;
    @SerializedName("full_names")
    @Expose
    private String fullNames;
    @SerializedName("status")
    @Expose
    private String status;

    public String getFingerprintToken() {
        return fingerprintToken;
    }

    public void setFingerprintToken(String fingerprintToken) {
        this.fingerprintToken = fingerprintToken;
    }

    public String getSubscriberMsidn() {
        return subscriberMsidn;
    }

    public void setSubscriberMsidn(String subscriberMsidn) {
        this.subscriberMsidn = subscriberMsidn;
    }

    public String getFullNames() {
        return fullNames;
    }

    public void setFullNames(String fullNames) {
        this.fullNames = fullNames;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
