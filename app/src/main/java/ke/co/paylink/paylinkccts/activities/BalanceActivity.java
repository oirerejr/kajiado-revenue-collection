package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivityBalanceBinding;
import ke.co.paylink.paylinkccts.fragments.QRCodeFragment;
import ke.co.paylink.paylinkccts.interfaces.IAgent;
import ke.co.paylink.paylinkccts.interfaces.IBalanceActivity;
import ke.co.paylink.paylinkccts.pos_api.POSNFCApi;
import ke.co.paylink.paylinkccts.retrofit.CardInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.BalanceResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vpos.apipackage.ByteUtil;
import vpos.apipackage.PosApiHelper;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;

public class BalanceActivity extends AppCompatActivity implements IBalanceActivity {

    ActivityBalanceBinding binding;
    private ObservableBoolean waitingMode = new ObservableBoolean(false);
//    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_balance);
        binding.setListener(this);
        binding.setWaitingMode(waitingMode);

//        final POSNFCApi.IPOSAPI iposapi = new POSNFCApi.IPOSAPI() {
//            @Override
//            public void onRead(String type, String uid) {
//                binding.cardNumber.setText(uid);
//                request.addProperty("account_number", uid);
//                request.addProperty("type", type);
//            }
//        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = -1;
                long time = System.currentTimeMillis();
                while (System.currentTimeMillis() < time + 60000) {
                    ret = readNfcCard();
                    if (ret == 0) {
                        break;
                    }
                }
            }
        }).start();
    }

    @Override
    public void ScanQRCode() {
        final QRCodeFragment qrCodeFragment = new QRCodeFragment();
        IAgent iAgent = new IAgent() {
            @Override
            public void OnResult(String result) {
                binding.cardNumber.setText(result);
                request.addProperty("account_number", result);
                request.addProperty("type", "NA");
                qrCodeFragment.dismiss();
            }
        };

        qrCodeFragment.setiAgent(iAgent);
        qrCodeFragment.show(getSupportFragmentManager(), QRCodeFragment.class.getSimpleName());
    }

    @Override
    public void OnClose() {
        finish();
    }

    JsonObject request = new JsonObject();

    @Override
    public void OnCheckBalance() {
        binding.amount.setText(null);
        String accountNumber = null;
        String pin = null;

        if (binding.cardNumber.getText() != null) {
            accountNumber = binding.cardNumber.getText().toString();
        }

        if (binding.pin.getText() != null) {
            pin = binding.pin.getText().toString();
        }

        if (TextUtils.isEmpty(accountNumber) || TextUtils.isEmpty(pin)) {
            Toast.makeText(this, "Must provide account number, pin number and agent number", Toast.LENGTH_SHORT).show();
            return;
        }

        waitingMode.set(true);

        request.addProperty("pin_number", pin);

        Call<BalanceResponse> balanceResponseCall = RetrofitSetup.createClass(CardInterface.class)
                .checkBalance(request);

        balanceResponseCall.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                waitingMode.set(false);
                BalanceResponse balanceResponse = response.body();
                if (balanceResponse != null) {
                    if (balanceResponse.getStatus().equals("200")) {
                        binding.setAmount(String.format("Ksh %s", balanceResponse.getAmountBalance()));
                    } else {
                        Toast.makeText(BalanceActivity.this, balanceResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        ke.co.paylink.paylinkccts.retrofit.models.Response response1 = new Gson().fromJson(response.errorBody().string(), ke.co.paylink.paylinkccts.retrofit.models.Response.class);
                        binding.amount.setText(response1.getStatusMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                waitingMode.set(false);
                Toast.makeText(BalanceActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private PosApiHelper posApiHelper = new PosApiHelper();

    public int readNfcCard() {
        byte[] NfcData_Len = new byte[5];
        byte[] Technology = new byte[25];
        byte[] NFC_UID = new byte[56];
        byte[] NDEF_message = new byte[500];

        int ret = posApiHelper.PiccNfc(NfcData_Len, Technology, NFC_UID, NDEF_message);

        int TechnologyLength = NfcData_Len[0] & 0xFF;
        int NFC_UID_length = NfcData_Len[1] & 0xFF;
        int NDEF_message_length = (NfcData_Len[3] & 0xFF) + (NfcData_Len[4] & 0xFF);
        byte[] NDEF_message_data = new byte[NDEF_message_length];
        byte[] NFC_UID_data = new byte[NFC_UID_length];
        System.arraycopy(NFC_UID, 0, NFC_UID_data, 0, NFC_UID_length);
        System.arraycopy(NDEF_message, 0, NDEF_message_data, 0, NDEF_message_length);
        String NDEF_message_data_str = new String(NDEF_message_data);
        String NDEF_str = null;
        if (!TextUtils.isEmpty(NDEF_message_data_str)) {
            NDEF_str = NDEF_message_data_str.substring(NDEF_message_data_str.indexOf("en") + 2, NDEF_message_data_str.length());
        }

        if (ret == 0) {
            posApiHelper.SysBeep();

            if (!TextUtils.isEmpty(NDEF_str)) {
                String type = new String(Technology).substring(0, TechnologyLength);
                final String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.cardNumber.setText(uid);
                    }
                });
                request.addProperty("account_number", uid);
                request.addProperty("type", type);
            } else {
                String type = new String(Technology).substring(0, TechnologyLength);
                final String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.cardNumber.setText(uid);
                    }
                });
                request.addProperty("account_number", uid);
                request.addProperty("type", type);
            }

        }
        return ret;
    }

}
