package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivityReverseBinding;
import ke.co.paylink.paylinkccts.fragments.QRCodeFragment;
import ke.co.paylink.paylinkccts.interfaces.IAgent;
import ke.co.paylink.paylinkccts.interfaces.IRefReg;
import ke.co.paylink.paylinkccts.pos_api.POSNFCApi;
import ke.co.paylink.paylinkccts.retrofit.CardInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.TransactionResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vpos.apipackage.ByteUtil;
import vpos.apipackage.PosApiHelper;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.JsonObject;

public class ReverseActivity extends AppCompatActivity implements IRefReg {

    private ActivityReverseBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reverse);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_reverse);
        binding.setListener(this);

//        final POSNFCApi.IPOSAPI iposapi = new POSNFCApi.IPOSAPI() {
//            @Override
//            public void onRead(String type, String uid) {
//                binding.accountNumber.setText(uid);
//                request.addProperty("nfc_number", uid);
//                request.addProperty("type", type);
//            }
//        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = -1;
                long time = System.currentTimeMillis();
                while (System.currentTimeMillis() < time + 60000) {
                    ret = readNfcCard();
                    if (ret == 0) {
                        break;
                    }
                }
            }
        }).start();

    }

    @Override
    public void OnActionClicked() {

        String accountNumber = null, transaction = null, pin = null;

        if (binding.accountNumber.getText() != null || !TextUtils.isEmpty(binding.accountNumber.getText().toString())) {
            accountNumber = binding.accountNumber.getText().toString();
        } else {
            binding.fieldLayout.setError("Please input account number");
            return;
        }
        if (binding.transaction.getText() != null || !TextUtils.isEmpty(binding.transaction.getText().toString())) {
            transaction = binding.transaction.getText().toString();
        } else {
            binding.fieldLayout.setError("Please input transaction number");
            return;
        }
        if (binding.pin.getText() != null || !TextUtils.isEmpty(binding.pin.getText().toString())) {
            pin = binding.pin.getText().toString();
        } else {
            binding.fieldLayout.setError("Please input your pin");
            return;
        }

        refund(transaction, pin);

    }

    @Override
    public void ScanQRCode() {
        final QRCodeFragment qrCodeFragment = new QRCodeFragment();
        IAgent iAgent = new IAgent() {
            @Override
            public void OnResult(String result) {
                binding.accountNumber.setText(result);
                request.addProperty("account_number", result);
                request.addProperty("type", "NA");
                qrCodeFragment.dismiss();
            }
        };

        qrCodeFragment.setiAgent(iAgent);
        qrCodeFragment.show(getSupportFragmentManager(), QRCodeFragment.class.getSimpleName());
    }

    @Override
    public void OnBack() {
        finish();
    }

    JsonObject request = new JsonObject();

    private void refund(String transactionId, String pin) {
        request.addProperty("trans_number", transactionId);
        request.addProperty("pin_number", pin);
        Call<TransactionResponse> refundResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "refund")
                .reverse(request);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Sending request...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        refundResponseCall.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    TransactionResponse transactionResponse = response.body();

                    if (transactionResponse == null) {
                        Toast.makeText(ReverseActivity.this, "Unauthorized request", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (transactionResponse.getStatus().equals("200")) {
                        finish();
                    }
                    Toast.makeText(ReverseActivity.this, transactionResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ReverseActivity.this, "Unauthorized request", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ReverseActivity.this, "Unauthorized request", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private PosApiHelper posApiHelper = new PosApiHelper();

    public int readNfcCard() {
        byte[] NfcData_Len = new byte[5];
        byte[] Technology = new byte[25];
        byte[] NFC_UID = new byte[56];
        byte[] NDEF_message = new byte[500];

        int ret = posApiHelper.PiccNfc(NfcData_Len, Technology, NFC_UID, NDEF_message);

        int TechnologyLength = NfcData_Len[0] & 0xFF;
        int NFC_UID_length = NfcData_Len[1] & 0xFF;
        int NDEF_message_length = (NfcData_Len[3] & 0xFF) + (NfcData_Len[4] & 0xFF);
        byte[] NDEF_message_data = new byte[NDEF_message_length];
        byte[] NFC_UID_data = new byte[NFC_UID_length];
        System.arraycopy(NFC_UID, 0, NFC_UID_data, 0, NFC_UID_length);
        System.arraycopy(NDEF_message, 0, NDEF_message_data, 0, NDEF_message_length);
        String NDEF_message_data_str = new String(NDEF_message_data);
        String NDEF_str = null;
        if (!TextUtils.isEmpty(NDEF_message_data_str)) {
            NDEF_str = NDEF_message_data_str.substring(NDEF_message_data_str.indexOf("en") + 2, NDEF_message_data_str.length());
        }

        if (ret == 0) {
            posApiHelper.SysBeep();

            if (!TextUtils.isEmpty(NDEF_str)) {
                String type = new String(Technology).substring(0, TechnologyLength);
                final String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.accountNumber.setText(uid);
                    }
                });
                request.addProperty("nfc_number", uid);
                request.addProperty("type", type);
            } else {
                String type = new String(Technology).substring(0, TechnologyLength);
                final String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.accountNumber.setText(uid);
                    }
                });
                request.addProperty("nfc_number", uid);
                request.addProperty("type", type);
            }

        }
        return ret;
    }


}
