package ke.co.paylink.paylinkccts.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.enums.TerminalOptions;
//import ke.co.paylink.paylinkccts.fp_module.authentec.SampleActivity;
import ke.co.paylink.paylinkccts.models.Option;
import ke.co.paylink.paylinkccts.observers.OAmount;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.adapters.CardOptionsAdapter;
import ke.co.paylink.paylinkccts.databinding.ActivityAmountBinding;
import ke.co.paylink.paylinkccts.interfaces.IAmountActivity;
import ke.co.paylink.paylinkccts.interfaces.ICardOptions;
import ke.co.paylink.paylinkccts.repository.Repository;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.gne.pm.PM;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

public class AmountActivity extends AppCompatActivity implements KeyboardVisibilityEventListener, IAmountActivity, ICardOptions, TextWatcher {

    private ActivityAmountBinding binding;
    private OAmount amountObservers = new OAmount();
    private CardOptionsAdapter cardOptionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_amount);
        KeyboardVisibilityEvent.setEventListener(this, this);

        cardOptionsAdapter = new CardOptionsAdapter(this, Repository.instance().getAgentOption());
        binding.setListener(this);
        binding.setAmountObserver(amountObservers);
        binding.setCurrency("Ksh");
        binding.agentOptionsSheet.agentOptions.setAdapter(cardOptionsAdapter);
        binding.amount.addTextChangedListener(this);

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onVisibilityChanged(boolean isOpen) {
        amountObservers.agentOptionBottomSheetHidable.set(isOpen);
        if (isOpen) {
            amountObservers.agentOptionBottomSheetState.set(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            amountObservers.agentOptionBottomSheetState.set(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    private final int AMOUNT_REQUEST = 10001;

    @Override
    public void OnProcessAmount() {
        Intent intent = new Intent(this, AgentActivity.class);
        intent.putExtra("amount", binding.amount.getText().toString());
        startActivityForResult(intent, AMOUNT_REQUEST);
    }

    @Override
    public void OnCardOptionSelected(Option option) {
        Intent intent = new Intent(this, RegisterCustomerActivity.class);
//        Intent intent = new Intent(this, RegisterCustomerActivity.class);

        switch (option.terminalOptions) {
            case REFUND:
                startActivity(new Intent(this, ReverseActivity.class));
                break;
            case REGISTER:
                startActivity(intent);
                break;
            case CHECK_BALANCE:
                startActivity(new Intent(this, BalanceActivity.class));
                break;
            case END_SESSION:
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AMOUNT_REQUEST) {
            binding.amount.requestFocus();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String amount = s.toString();

        try {
            float amountV = Float.parseFloat(amount);
            if (amountV > 0) {
                amountObservers.showNext.set(true);
            } else {
//                binding.amount.setText(null);
                amountObservers.showNext.set(false);
            }

        } catch (NumberFormatException e) {
//            binding.amount.setText(null);
            amountObservers.showNext.set(false);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
