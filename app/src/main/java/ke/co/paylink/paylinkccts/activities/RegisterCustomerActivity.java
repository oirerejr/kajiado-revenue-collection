package ke.co.paylink.paylinkccts.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivityRegisterCardBinding;
//import ke.co.paylink.paylinkccts.fp_test.pm.PM;
import ke.co.paylink.paylinkccts.fp.Globals;
import ke.co.paylink.paylinkccts.fragments.QRCodeFragment;
import ke.co.paylink.paylinkccts.interfaces.IAgent;
import ke.co.paylink.paylinkccts.interfaces.IRefReg;

import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.gne.pm.PM;

import ke.co.paylink.paylinkccts.retrofit.CardInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.RegisterResponse;
import plfm.enrollment.Enrollment;
import utils.DBManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vpos.apipackage.ByteUtil;
import vpos.apipackage.PosApiHelper;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.digitalpersona.uareu.Reader.Priority;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.Arrays;

public class RegisterCustomerActivity extends AppCompatActivity implements IRefReg {

    /*Fingerprint

    Start*/

    public static final int REQUEST_CODE = 1;

    private final int GENERAL_ACTIVITY_RESULT = 1;

    private static final String ACTION_USB_PERMISSION = "com.digitalpersona.uareu.dpfpddusbhost.USB_PERMISSION";

    private ReaderCollection readers;

    Reader m_reader;

    private String deviceName = "";

    private int m_current_fmds_count = 0;
    private boolean m_first = true;
    private boolean m_success = false;
    private Fmd m_enrollment_fmd = null;
    private int m_templateSize = 0;
    //    EnrollmentCallback enrollThread = null;
    private Reader.CaptureResult cap_result = null;
    private String m_textString;
    private String m_text_conclusionString;
    private Engine m_engine = null;
    private boolean m_reset = false;
    private int m_DPI = 0;
    private Bitmap m_bitmap = null;
    private String m_enginError;

    /* Fingerprint End*/

    private ActivityRegisterCardBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_card);
        binding.setListener(this);
        deviceName = PLSharedPrefs.get().getFPDevice();

//        PM.powerOn();

        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = -1;
                long time = System.currentTimeMillis();
                while (System.currentTimeMillis() < time + 120000) {
                    ret = readNfcCard();
                    if (ret == 0) {
                        break;
                    }
                }
            }
        }).start();

        initializeDPSKD();

    }

    //    public void OnScanFinger() {
//
//        deviceName = PLSharedPrefs.get().getFPDevice();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Enrollment.get(RegisterCustomerActivity.this, deviceName, new Enrollment.IEnrollment() {
//                    @Override
//                    public void onEnroll(final byte[] data) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                String strStore = Base64.encodeToString(data, Base64.DEFAULT);
//                                binding.scanButton.setText("Created fingerprint successfully");
//                                request.addProperty("fingerprint_token", strStore);
//                                request.addProperty("fingerprint_type", String.valueOf(1));
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onTerminate(final String error) {
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
////                                Toast.makeText(RegisterCustomerActivity.this, String.format("Error %s", error), Toast.LENGTH_LONG).show();
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onText(final String textConclusion, final String mTextString) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                binding.scanButton.setText(mTextString);
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onStep(final String step) {
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                            }
//                        });
//                    }
//                });
//            }
//        }).start();
//
//    }
    int enableFingerprintTrial = 0;
    CaptureThread captureThread = null;

    private void initializeDPSKD() {
        // initiliaze dp sdk
        try {
            Context applContext = getApplicationContext();
            m_reader = Globals.getInstance().getReader(deviceName, applContext);
            m_reader.Open(Reader.Priority.EXCLUSIVE);
            m_DPI = Globals.GetFirstDPI(m_reader);
            m_engine = UareUGlobal.GetEngine();
//            binding.fingerStat.setText("Fingerprint enabled");
            synchronized (this) {
                if (null != captureThread && !captureThread.isThreadFinished()) {
                    Toast.makeText(applContext, "Thread still running", Toast.LENGTH_SHORT).show();
                    return;
                }
                // loop capture on a separate thread to avoid freezing the UI
                captureThread = new CaptureThread();
                captureThread.start();
            }
        } catch (Exception e) {
//            binding.fingerStat.setText("Error during init of reader");
//            m_deviceName = "";
            onBackPressed();
            if (enableFingerprintTrial != 3) {
                initializeDPSKD();
                enableFingerprintTrial++;
            }
            return;
        }
    }

    @Override
    protected void onDestroy() {
        //power off
//        PM.powerOff();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        uninitialize();
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
//        if (hasFocus) {
//            animateCard();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void OnActionClicked() {

        String fullName = null, phoneNumber = null, pin = null, cardNumber = null;

        if (binding.cardNumber.getText() != null || !TextUtils.isEmpty(binding.cardNumber.getText().toString())) {
            fullName = binding.cardNumber.getText().toString();
        } else {
            binding.fieldLayout.setError("Please input full name");
            return;
        }
        if (binding.cardHolder.getText() != null || !TextUtils.isEmpty(binding.cardHolder.getText().toString())) {
            phoneNumber = binding.cardHolder.getText().toString();
        } else {
            binding.fieldLayout.setError("Please input phone number");
            return;
        }
        if (binding.cardDet.getText() != null || !TextUtils.isEmpty(binding.cardDet.getText().toString())) {
            cardNumber = binding.cardDet.getText().toString();
        } else {
            binding.fieldLayout.setError("Please scan/tap card");
            return;
        }
        if (binding.field.getText() != null || !TextUtils.isEmpty(binding.field.getText().toString())) {
            pin = binding.field.getText().toString();
        } else {
            binding.fieldLayout.setError("Please input your pin");
            return;
        }

        registerCard(fullName, phoneNumber, pin);

    }

    public static double getCardNumber() {
        long max = 111111111, min = 555555555;
        double x = (int) (Math.random() * ((min - max) + 1)) + min;
        return x;
    }

    @Override
    public void ScanQRCode() {
        final QRCodeFragment qrCodeFragment = new QRCodeFragment();
        IAgent iAgent = new IAgent() {
            @Override
            public void OnResult(String result) {
                binding.cardDet.setText(result);
                request.addProperty("card_number", result);
                request.addProperty("type", "NA");
                qrCodeFragment.dismiss();
            }
        };

        qrCodeFragment.setiAgent(iAgent);
        qrCodeFragment.show(getSupportFragmentManager(), QRCodeFragment.class.getSimpleName());
    }

    @Override
    public void OnBack() {
        onBackPressed();
    }

    JsonObject request = new JsonObject();

    private void registerCard(String fullName, String phoneNumber, String pin) {
        request.addProperty("full_names", fullName);
        request.addProperty("subscriber_msidn", phoneNumber);
        request.addProperty("pin_number", pin);
        Call<RegisterResponse> registerResponseCall = RetrofitSetup.createTransactClass(CardInterface.class, "register-card")
                .registerCard(request);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Registering customer...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        registerResponseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                progressDialog.dismiss();
                RegisterResponse registerResponse = response.body();
                if (registerResponse == null) {
                    try {
                        ke.co.paylink.paylinkccts.retrofit.models.Response error = new Gson().fromJson(response.errorBody().string(), ke.co.paylink.paylinkccts.retrofit.models.Response.class);
                        Toast.makeText(RegisterCustomerActivity.this, error.getStatusMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                }
                if (registerResponse.getStatus().equals("200")) {
                    onBackPressed();
                }
                Toast.makeText(RegisterCustomerActivity.this, registerResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RegisterCustomerActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private PosApiHelper posApiHelper = new PosApiHelper();

    public int readNfcCard() {
        byte[] NfcData_Len = new byte[5];
        byte[] Technology = new byte[25];
        byte[] NFC_UID = new byte[56];
        byte[] NDEF_message = new byte[500];

        int ret = posApiHelper.PiccNfc(NfcData_Len, Technology, NFC_UID, NDEF_message);

        int TechnologyLength = NfcData_Len[0] & 0xFF;
        int NFC_UID_length = NfcData_Len[1] & 0xFF;
        int NDEF_message_length = (NfcData_Len[3] & 0xFF) + (NfcData_Len[4] & 0xFF);
        byte[] NDEF_message_data = new byte[NDEF_message_length];
        byte[] NFC_UID_data = new byte[NFC_UID_length];
        System.arraycopy(NFC_UID, 0, NFC_UID_data, 0, NFC_UID_length);
        System.arraycopy(NDEF_message, 0, NDEF_message_data, 0, NDEF_message_length);
        String NDEF_message_data_str = new String(NDEF_message_data);
        String NDEF_str = null;
        if (!TextUtils.isEmpty(NDEF_message_data_str)) {
            NDEF_str = NDEF_message_data_str.substring(NDEF_message_data_str.indexOf("en") + 2, NDEF_message_data_str.length());
        }

        if (ret == 0) {
            posApiHelper.SysBeep();

            if (!TextUtils.isEmpty(NDEF_str)) {
                String type = new String(Technology).substring(0, TechnologyLength);
                final String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.cardDet.setText(uid);
                    }
                });
                request.addProperty("nfc_number", uid);
                request.addProperty("type", type);
            } else {
                String type = new String(Technology).substring(0, TechnologyLength);
                final String uid = ByteUtil.bytearrayToHexString(NFC_UID_data, NFC_UID_data.length);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.cardDet.setText(uid);
                    }
                });
                request.addProperty("nfc_number", uid);
                request.addProperty("type", type);
            }

        }
        return ret;
    }

    @Override
    protected void onStop() {
        binding.scanButton.setText("Device: (No Reader Selected)");
        super.onStop();
    }

    private boolean m_bThreadFinished = false;
    private String mTextConclusionString;
    private Engine mEngine = null;
    private int mCurrentFmdsCount = 0;
    private String mTextString;
    private boolean mFirst = true;
    private boolean mSuccess = false;
    private Fmd mEnrollmentFmd = null;
    private int mTemplateSize = 0;
    Engine.EnrollmentCallback enrollThread = null;
    private Reader.CaptureResult capResult = null;

    public class CaptureThread extends Thread {

        public boolean isThreadFinished() {
            return m_bThreadFinished;
        }

        public void run() {
            super.run();
            synchronized (this) {
                m_bThreadFinished = false;

                try {
//                    while (!m_reset) {

                    /*
                     *Capture Finger Print 采集特征
                     *可根据传递的值，获取不同标准的指纹模板
                     *Different standard fingerprint templates can be obtained based on the passed values
                     */
                    capResult = m_reader.Capture(Fid.Format.ISO_19794_4_2005, Globals.DefaultImageProcessing, m_DPI, -1);
                    Engine m_engine = UareUGlobal.GetEngine();
                    Fmd m_fmd = m_engine.CreateFmd(capResult.image, Fmd.Format.ISO_19794_2_2005);
                    if (m_fmd.getData().length > 0) {
                        byte[] data = m_fmd.getData();
                        String strStore = Base64.encodeToString(data, Base64.DEFAULT);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                binding.scanButton.setText("Created fingerprint template");
                            }
                        });
                        request.addProperty("fingerprint_token", strStore);
                        uninitialize();
                    }

                    /******************************************************/
                    /***********add by liuhao 20180522 START***************/
                    /*
                     *When you save the retrieved fingerprint ISO / ANSI bytes for database storage, you need to
                     *convert them to the basic format supported by the database. We recommend that you always  *convert them to Base64
                     *将获取到的指纹ISO / ANSI格式bytes进行数据库保存时，需要转换成数据库支持的基本格式，我们建议一定要转换成Base64
                     */

                    //Store to database 存储到数据库

//                    setValue(FID);
                    /***********add by liuhao 20180522 END***************/
                    /******************************************************/
//                   /*
//                    Use Globals.GetBitmapFromRaw from raw to bitmap
//                    通过Globals.GetBitmapFromRaw 转换成bitmap
//                    */
//                    m_bitmap = Globals.GetBitmapFromRaw(cap_result.image.getViews()[0].getImageData(), cap_result.image.getViews()[0].getWidth(), cap_result.image.getViews()[0].getHeight());
//                    m_text_conclusionString = Globals.QualityToString(cap_result);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            binding.scanButton.setText(m_text_conclusionString);
//                        }
//                    });
//                    }
                } catch (final UareUException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            binding.scanButton.setText(e.toString());
                        }
                    });
                    uninitialize();
                }
                m_bThreadFinished = true;
            }
        }

    }

    private void uninitialize() {
        try {
            m_reset = true;
            try {
                m_reader.CancelCapture();
            } catch (Exception e) {
            }
            m_reader.Close();
        } catch (Exception e) {
            Log.w("UareUSampleJava", "error during Reader shutdown");
        }
        captureThread = null;
    }

}
