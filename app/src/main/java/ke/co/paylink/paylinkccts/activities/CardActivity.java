package ke.co.paylink.paylinkccts.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.card_operations.Card;
import ke.co.paylink.paylinkccts.databinding.ActivityCardBinding;
import ke.co.paylink.paylinkccts.interfaces.ICardActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.google.gson.JsonObject;

public class CardActivity extends AppCompatActivity implements ICardActivity {

    ActivityCardBinding binding;
    private ObservableBoolean fullView = new ObservableBoolean(true);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_card);

        Intent intent = getIntent();
        binding.amount.setText(intent.getStringExtra("amount"));
        String card = intent.getStringExtra("card");
        binding.setCard(card);
//        binding.setWidth((float) width);
        binding.setListener(this);
        binding.setFullView(fullView);
    }

    @Override
    public void OnCardClicked() {
        fullView.set(!fullView.get());
        int width = 0;
        if (!fullView.get()) {
            width = getWindow().getDecorView().getWidth() / 2;
        }
        ObjectAnimator animation = ObjectAnimator.ofFloat(binding.cardView, "translationX", (float) width);
        animation.setDuration(1000);
        animation.start();
    }

    @Override
    public void OnCancelTransaction() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void ScanQRCode() {

    }

    @Override
    public void OnChangeAmount() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void OnAcceptPayment() {
//        JsonObject request = new JsonObject();
//        Card.instance().makePayment();


//        startActivity(new Intent(this, ConfirmationActivity.class));
    }
}
