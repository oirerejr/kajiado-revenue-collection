package ke.co.paylink.paylinkccts.interfaces;

import ke.co.paylink.paylinkccts.models.Option;

public interface ICardOptions {

    void OnCardOptionSelected(Option option);

}
