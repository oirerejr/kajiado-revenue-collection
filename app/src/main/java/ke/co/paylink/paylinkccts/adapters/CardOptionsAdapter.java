package ke.co.paylink.paylinkccts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ItemCardOptionBinding;
import ke.co.paylink.paylinkccts.interfaces.ICardOptions;
import ke.co.paylink.paylinkccts.models.Option;

public class CardOptionsAdapter extends RecyclerView.Adapter<CardOptionsAdapter.ViewHolder> {

    private Context context;
    private List<Option> options;
    private ICardOptions iCardOptions;

    public CardOptionsAdapter(Context context, List<Option> options) {
        this.context = context;
        this.options = options;
        iCardOptions = (ICardOptions) context;
    }

    @NonNull
    @Override
    public CardOptionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card_option, parent, false);
        ItemCardOptionBinding binding = ItemCardOptionBinding.bind(view);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CardOptionsAdapter.ViewHolder holder, int position) {
        Option option = options.get(position);
        holder.bind(option);
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemCardOptionBinding binding;

        public ViewHolder(@NonNull ItemCardOptionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(Option option) {
            binding.setOption(option);
            binding.setCardListener(iCardOptions);
            binding.executePendingBindings();
        }

    }

}
