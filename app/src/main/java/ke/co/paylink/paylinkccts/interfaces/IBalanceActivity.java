package ke.co.paylink.paylinkccts.interfaces;

public interface IBalanceActivity {

    void OnClose();
    void OnCheckBalance();
    void ScanQRCode();

}
