package ke.co.paylink.paylinkccts.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivityPosdataBinding;
import ke.co.paylink.paylinkccts.fp.Globals;
import ke.co.paylink.paylinkccts.interfaces.IPosData;
import ke.co.paylink.paylinkccts.observers.OPOSData;
import ke.co.paylink.paylinkccts.view_models.LocationViewModel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.dpfpddusbhost.DPFPDDUsbException;
import com.digitalpersona.uareu.dpfpddusbhost.DPFPDDUsbHost;
import com.gne.pm.PM;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.UUID;

import static ke.co.paylink.paylinkccts.activities.RegisterCustomerActivity.REQUEST_CODE;

public class POSDataActivity extends AppCompatActivity implements IPosData {

    public static String[] MY_PERMISSIONS = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.MOUNT_UNMOUNT_FILESYSTEMS",
            "android.permission.WRITE_OWNER_DATA",
            "android.permission.READ_OWNER_DATA",
            "android.hardware.usb.accessory",
            "com.digitalpersona.uareu.dpfpddusbhost.USB_PERMISSION",
            "android.permission.HARDWARE_TEST",
            "android.permission.READ_PHONE_STATE",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.hardware.usb.host"
    };

    public static final int REQUEST_CODE = 1;

    private final int GENERAL_ACTIVITY_RESULT = 1;

    private static final String ACTION_USB_PERMISSION = "ke.co.paylink.paylinkccts.USB_PERMISSION";

    private String m_deviceName = "";

    Reader m_reader;

    ActivityPosdataBinding binding;
    private final int NORMAL = 0, PHONE_STATE = 1, LOCATION = 2, GPS = 3;
    private String[] messages = new String[]{"Connecting you to \nServer", "Allow phone state permission so we know which device we are registering", "Allow location permission so we know where your device is", "Turn on location permission so we know where your device is"};
    private OPOSData oposData;
    private final int READ_PHONE_STATE = 1888;
    private final int ACCESS_FINE_LOCATION = 3555;
    private final int ALL_PERMISSION_REQUIRED = 5555;
    private TelephonyManager telephonyManager;
    public static final int REQUEST_CHECK_SETTINGS = 3001;

    private Bundle savedInstanceState = null;

//    ArrayList<String> permissionsToRequest = new ArrayList<>();
//    ArrayList<String> permissionsRejected = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_posdata);
        binding.setListener(this);
        oposData = new OPOSData();
        binding.setObserver(oposData);
        binding.setMessage(messages[NORMAL]);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                // 没有写文件的权限，去申请读写文件的权限，系统会弹出权限许可对话框
                //Without the permission to Write, to apply for the permission to Read and Write, the system will pop up the permission dialog
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_CODE);
            } else {
                init();
            }
        } else {
            init();
        }

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(4000);
//                    Intent intent = new Intent(POSDataActivity.this, SignInActivity.class);
////                    intent.putExtra("location", location);
//                    intent.putExtra("device", UUID.randomUUID().toString());
//                    startActivity(intent);
//                    finish();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                    Intent intent = new Intent(POSDataActivity.this, SignInActivity.class);
////                    intent.putExtra("location", location);
//                    intent.putExtra("device", UUID.randomUUID().toString());
//                    startActivity(intent);
//                    finish();
//                }
//            }
//        }).start();

        //get all permission requests
//        ArrayList<String> permissions = new ArrayList<>();
//        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
//        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
//
//        permissionsToRequest = permissionsToRequest(permissions);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (permissionsToRequest.size() > 0) {
//                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSION_REQUIRED);
//            }else {
//                getDeviceInformation();
//            }
//        } else {
//            getDeviceInformation();
//        }
    }

    private void init() {
        //enable tracing
        System.setProperty("DPTRACE_ON", "1");
        //power on
        PM.powerOn();

        getFingerprintReader();
    }

    private ReaderCollection readers;

    private void getFingerprintReader() {
        // initiliaze dp sdk
        try {
            Context applContext = getApplicationContext();
            readers = Globals.getInstance().getReaders(applContext);
        } catch (UareUException e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
//            onBackPressed();
        }

        int nSize = readers.size();
        m_deviceName = nSize == 0 ? "" : readers.get(0).GetDescription().name;

        if ((m_deviceName != null) && !m_deviceName.isEmpty()) {

            try {
                Context applContext = getApplicationContext();
                m_reader = Globals.getInstance().getReader(m_deviceName, applContext);

                {
                    PendingIntent mPermissionIntent;
                    mPermissionIntent = PendingIntent.getBroadcast(applContext, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                    applContext.registerReceiver(mUsbReceiver, filter);

                    if (DPFPDDUsbHost.DPFPDDUsbCheckAndRequestPermissions(applContext, mPermissionIntent, m_deviceName)) {
                        CheckDevice();
                    }
                }
            } catch (UareUException e1) {
                showReaderNotFound();
            } catch (DPFPDDUsbException e) {
                showReaderNotFound();
            }

        } else {
            showReaderNotFound();
        }

    }

    private void showReaderNotFound() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Reader Not Found");
        alertDialogBuilder.setMessage("Plug in a Reader and try again.").setCancelable(false).setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        //power off
//        PM.powerOff();
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        onCreate(savedInstanceState);
        super.onConfigurationChanged(newConfig);
    }

//    private void requestLocationPermission() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            permissionsToRequest = permissionsToRequest(permissionsToRequest);
//            if (permissionsToRequest.size() > 0) {
//                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSION_REQUIRED);
//            } else {
//                getDeviceInformation();
//            }
//        }
//    }

//    private ArrayList<String> permissionsToRequest(ArrayList<String> permissions) {
//        ArrayList<String> result = new ArrayList<>();
//        for (String perm : permissions) {
//            if (!hasPermission(perm)) {
//                result.add(perm);
//            }
//        }
//
//        return result;
//    }

//    private boolean hasPermission(String permission) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
//        }
//        return true;
//    }

    @Override
    protected void onStart() {
        super.onStart();
//        binding.loader.setBackgroundResource(R.drawable.loader_animation_list);
//        AnimationDrawable animationDrawable = (AnimationDrawable) binding.loader.getBackground();
//        animationDrawable.start();
    }

//    private void getDeviceInformation() {
//        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
//        if (!isLocationAccessGranted() || permissionsRejected.size() > 0) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
//            return;
//        }
//        checkGpsStatus();
//    }

    private void getDeviceLocation() {
        if (!isLocationAccessGranted()) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
            return;
        }

        LocationViewModel locationViewModel = ViewModelProviders.of(this).get(LocationViewModel.class);
        locationViewModel.getModel(this).observe(this, new Observer<Location>() {
            @Override
            public void onChanged(Location location) {
                connectToPaylink(location);
            }
        });
    }

    @SuppressLint("hardwareIds")
    private void connectToPaylink(Location location) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
            return;
        }
        Intent intent = new Intent(this, SignInActivity.class);
        intent.putExtra("location", location);
        intent.putExtra("device", telephonyManager.getDeviceId());
        startActivity(intent);
        finish();
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case ALL_PERMISSION_REQUIRED:
//                permissionsRejected.clear();
//                for (String perm : permissionsToRequest) {
//                    if (!hasPermission(perm)) {
//                        permissionsRejected.add(perm);
//                    }
//                }
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (permissionsRejected.size() > 0) {
//                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
//                            binding.setMessage("These permissions are mandatory to get your location. You need to allow them.");
//                            oposData.requestState.set(true);
//                            return;
//                        }
//                    } else {
////                        LocationViewModel locationViewModel = ViewModelProviders.of(this).get(LocationViewModel.class);
////                        locationViewModel.getModel(this).observe(this, new Observer<Location>() {
////                            @Override
////                            public void onChanged(Location location) {
////                                connectToPaylink(location);
////                            }
////                        });
//                        getDeviceInformation();
////                        checkGpsStatus();
//                    }
//
//                } else {
//                    // connect google client
//                    checkGpsStatus();
//
//                }
//                break;
//            case READ_PHONE_STATE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if (isLocationAccessGranted()) {
//                        checkGpsStatus();
//                    } else {
//                        binding.setMessage(messages[PHONE_STATE]);
//                        oposData.requestState.set(true);
//                    }
//
//                    if (permissionsRejected.size() == 0) {
//                        checkGpsStatus();
//                    } else {
//                        binding.setMessage(messages[PHONE_STATE]);
//                        oposData.requestState.set(true);
//                    }
//                }
//                break;
//            case ACCESS_FINE_LOCATION:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if (!isLocationAccessGranted()) {
//                        binding.setMessage(messages[LOCATION]);
//                        oposData.requestState.set(true);
//                    } else {
//                        checkGpsStatus();
//                    }
//                }
//                break;
//        }
//
////        switch (requestCode) {
////            case READ_PHONE_STATE:
////                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    if (isLocationAccessGranted()) {
////                        checkGpsStatus();
////                    } else {
////                        binding.setMessage(messages[PHONE_STATE]);
////                        oposData.requestState.set(true);
////                    }
////                }
////                break;
////            case ACCESS_FINE_LOCATION:
////                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    if (!isLocationAccessGranted()) {
////                        binding.setMessage(messages[LOCATION]);
////                        oposData.requestState.set(true);
////                    } else {
////                        checkGpsStatus();
////                    }
////                }
////                break;
////        }
//    }

    @Override
    public void OnAllowAccess() {
        binding.setMessage(messages[NORMAL]);
        oposData.requestState.set(false);

//        requestLocationPermission();

        if (!isLocationAccessGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
        } else if (!isPhoneAccessGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
        } else {
            checkGpsStatus();
        }
    }

    private boolean isLocationAccessGranted() {

        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

    }

    private boolean isPhoneAccessGranted() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
    }

    private void checkGpsStatus() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);

        LocationSettingsRequest.Builder locationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(locationSettingsRequest.build());

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                        resolvableApiException.startResolutionForResult(POSDataActivity.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getDeviceLocation();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode != RESULT_OK) {
            binding.setMessage(messages[GPS]);
            oposData.requestState.set(true);
        } else {
            getDeviceLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "no permission ,plz to request~", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_CODE);
            } else {
                init();
            }
        }
    }

    protected void CheckDevice() {
        PLSharedPrefs.get().setFPDevice(m_deviceName);
        try {
            m_reader.Open(Reader.Priority.EXCLUSIVE);
            Reader.Capabilities cap = m_reader.GetCapabilities();
            m_reader.Close();
            proceed();
        } catch (UareUException e1) {
            showReaderNotFound();
        }
    }

    private void proceed() {
        Intent intent = new Intent(POSDataActivity.this, SignInActivity.class);
//                    intent.putExtra("location", location);
        intent.putExtra("device", UUID.randomUUID().toString());
        startActivity(intent);
        finish();
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            //call method to set up device communication
                            CheckDevice();
                        }
                    } else {
                        Toast.makeText(context, "Permission denied USB", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    };

}
