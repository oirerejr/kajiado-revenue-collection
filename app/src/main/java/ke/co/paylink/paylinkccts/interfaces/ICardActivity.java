package ke.co.paylink.paylinkccts.interfaces;

public interface ICardActivity {

    void OnCardClicked();
    void OnCancelTransaction();
    void ScanQRCode();
    void OnChangeAmount();
    void OnAcceptPayment();

}
