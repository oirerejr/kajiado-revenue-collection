package ke.co.paylink.paylinkccts.card_operations;

import com.google.gson.JsonObject;

import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.retrofit.CardInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.PaymentResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Card {

    private static Card card;

    private Card() {
    }

    public static synchronized Card instance() {
        if (card == null) {
            card = new Card();
        }
        return card;
    }

    // Make Payment - card amount
    // header - TID, token
//    public void makePayment(JsonObject request) {
//        Call<PaymentResponse> responseCall = RetrofitSetup.createClass(CardInterface.class, PLSharedPrefs.get().getUser().getToken()).makePayment(request);
//        responseCall.enqueue(new Callback<PaymentResponse>() {
//            @Override
//            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<PaymentResponse> call, Throwable t) {
//
//            }
//        });
//    }

}
