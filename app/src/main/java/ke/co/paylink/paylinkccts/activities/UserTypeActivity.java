package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.adapters.UserTypeAdapter;
import ke.co.paylink.paylinkccts.databinding.ActivityUserTypeBinding;

import android.os.Bundle;

public class UserTypeActivity extends AppCompatActivity {

    ActivityUserTypeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_type);
        UserTypeAdapter adapter = new UserTypeAdapter(this);
        binding.types.setAdapter(adapter);
    }

}
