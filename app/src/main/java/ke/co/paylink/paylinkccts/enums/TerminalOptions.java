package ke.co.paylink.paylinkccts.enums;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

public enum TerminalOptions implements Parcelable {
    MAKE_PAYMENT,
    TOP_UP_CARD,
    CANCEL,
    TRANSFER_TO_OTHER_CARD,
    END_SESSION,
    CHECK_BALANCE,
    REGISTER,
    REFUND,
    WITHDRAW;

    private static final String name = TerminalOptions.class.getName();

    public void attachTo(Intent intent) {
        intent.putExtra(name, ordinal());
    }

    public static TerminalOptions detachFrom(Intent intent) {
        if (!intent.hasExtra(name)) throw new IllegalStateException();
        return values()[intent.getIntExtra(name, -1)];
    }

    public static final Creator<TerminalOptions> CREATOR = new Creator<TerminalOptions>() {
        @Override
        public TerminalOptions createFromParcel(Parcel in) {
            return TerminalOptions.values()[in.readInt()];
        }

        @Override
        public TerminalOptions[] newArray(int size) {
            return new TerminalOptions[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());
    }
}