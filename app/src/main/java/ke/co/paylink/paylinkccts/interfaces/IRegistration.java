package ke.co.paylink.paylinkccts.interfaces;

public interface IRegistration {

    void OnCompleteRegistration();

    void OnSelectService();

}
