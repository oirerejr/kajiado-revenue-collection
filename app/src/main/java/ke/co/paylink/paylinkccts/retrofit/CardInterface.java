package ke.co.paylink.paylinkccts.retrofit;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import ke.co.paylink.paylinkccts.retrofit.models.BalanceResponse;
import ke.co.paylink.paylinkccts.retrofit.models.MemberResponse;
import ke.co.paylink.paylinkccts.retrofit.models.RefundResponse;
import ke.co.paylink.paylinkccts.retrofit.models.RegisterResponse;
import ke.co.paylink.paylinkccts.retrofit.models.TransactionResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CardInterface {

    //    Call<PaymentResponse> makePayment(@Body JsonObject payment);
    @POST("register")
    Call<RegisterResponse> registerCard(@Body JsonObject card);

    @POST("refund")
    Call<RefundResponse> refund(@Body JsonObject payment);

    @POST("balance")
    Call<BalanceResponse> checkBalance(@Body JsonObject card);

    @POST("channels")
    Call<TransactionResponse> reverse(@Body JsonObject transaction);

    @POST("payment")
    Call<TransactionResponse> payment(@Body JsonObject card);

    @POST("top-up")
    Call<TransactionResponse> topUp(@Body JsonObject card);

    @POST("member")
    Call<MemberResponse> member(@Body JsonObject card);

}
