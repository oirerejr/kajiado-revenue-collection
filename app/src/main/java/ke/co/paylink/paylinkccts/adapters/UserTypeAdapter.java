package ke.co.paylink.paylinkccts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ke.co.paylink.paylinkccts.databinding.ItemUserTypeBinding;
import ke.co.paylink.paylinkccts.models.UserType;
import ke.co.paylink.paylinkccts.repository.UserTypeRepo;

public class UserTypeAdapter extends RecyclerView.Adapter<UserTypeAdapter.ViewHolder> {

    private Context context;
    private List<UserType> userTypes;
    private int lastSelectedIndex = 0;

    public interface IUserTypeAdapter {
        void OnUserTypeSelected(int position);
    }

    private IUserTypeAdapter iUserTypeAdapter = new IUserTypeAdapter() {
        @Override
        public void OnUserTypeSelected(int position) {
            lastSelectedIndex = position;
            notifyDataSetChanged();
        }
    };

    public UserTypeAdapter(Context context) {
        this.context = context;
        this.userTypes = UserTypeRepo.instance().getUserTypes();
    }

    @NonNull
    @Override
    public UserTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserTypeBinding binding = ItemUserTypeBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserTypeAdapter.ViewHolder holder, int position) {
        UserType userType = userTypes.get(position);
        holder.bind(userType, lastSelectedIndex == position);
    }

    @Override
    public int getItemCount() {
        return userTypes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemUserTypeBinding binding;

        public ViewHolder(@NonNull ItemUserTypeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bind(UserType userType, boolean selected) {
            binding.setUserType(userType);
            binding.setSelected(selected);
            binding.setPosition(getAdapterPosition());
            binding.setListener(iUserTypeAdapter);
            binding.executePendingBindings();
        }
    }

}
