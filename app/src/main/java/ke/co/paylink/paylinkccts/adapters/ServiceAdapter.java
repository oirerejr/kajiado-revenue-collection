package ke.co.paylink.paylinkccts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ke.co.paylink.paylinkccts.databinding.ItemBankBinding;
import ke.co.paylink.paylinkccts.databinding.LayoutTextviewBinding;
import ke.co.paylink.paylinkccts.interfaces.IBankAdapter;
import ke.co.paylink.paylinkccts.interfaces.IServiceAdapter;
import ke.co.paylink.paylinkccts.models.Bank;
import ke.co.paylink.paylinkccts.retrofit.models.RevenueType;
import ke.co.paylink.paylinkccts.retrofit.models.Service;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private Context context;
    private List<RevenueType> revenueTypes;
    private IServiceAdapter iServiceAdapter;

    public ServiceAdapter(Context context, List<RevenueType> revenueTypes) {
        this.context = context;
        this.revenueTypes = revenueTypes;
        iServiceAdapter = (IServiceAdapter) context;
    }

    @NonNull
    @Override
    public ServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutTextviewBinding textviewBinding = LayoutTextviewBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ServiceAdapter.ViewHolder(textviewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceAdapter.ViewHolder holder, int position) {
        RevenueType type = revenueTypes.get(position);
        holder.bind(type);
    }

    @Override
    public int getItemCount() {
        return revenueTypes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LayoutTextviewBinding binding;

        public ViewHolder(@NonNull LayoutTextviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(RevenueType type) {
            binding.setRevenue(type);
            binding.setListener(iServiceAdapter);
            binding.executePendingBindings();
        }

    }

}
