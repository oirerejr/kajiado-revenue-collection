package ke.co.paylink.paylinkccts;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import ke.co.paylink.paylinkccts.models.User;
import ke.co.paylink.paylinkccts.retrofit.models.LoginResponse;

public class PLSharedPrefs {

    private static PLSharedPrefs sharedPrefs;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final Gson gson = new Gson();
    private final String SHARED_PREFS = "paylink_ccts_shared_prefs";
    private final String USER = "user";
    private final String TERMINAL = "terminal";
    private final String FPDEVICE = "fp_device";

    public static PLSharedPrefs get() {
        if (sharedPrefs == null) {
            sharedPrefs = new PLSharedPrefs();
        }
        return sharedPrefs;
    }

    private PLSharedPrefs() {
        sharedPreferences = Paylink.instance().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setTerminal(String terminal) {
        editor.putString(TERMINAL, terminal);
        editor.apply();
    }

    public String getTerminal() {
        return sharedPreferences.getString(TERMINAL, "");
    }

    public void setFPDevice(String terminal) {
        editor.putString(FPDEVICE, terminal);
        editor.apply();
    }

    public String getFPDevice() {
        return sharedPreferences.getString(FPDEVICE, "");
    }

    public void setUser(LoginResponse user) {
        editor.putString(USER, gson.toJson(user));
        editor.apply();
    }

    public LoginResponse getUser() {
        String userStr = sharedPreferences.getString(USER, "");
        LoginResponse user = null;
        if (userStr != null) {
            user = gson.fromJson(userStr, LoginResponse.class);
        }
        return user;
    }

}
