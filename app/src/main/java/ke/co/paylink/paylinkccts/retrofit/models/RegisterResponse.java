package ke.co.paylink.paylinkccts.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse extends Response {
    @SerializedName("subscriber_msidn")
    @Expose
    private String subscriberMsidn;
    @SerializedName("account_number")
    @Expose
    private Integer accountNumber;
    @SerializedName("token_string")
    @Expose
    private String tokenString;
    @SerializedName("agent_number")
    @Expose
    private String agentNumber;

    public String getSubscriberMsidn() {
        return subscriberMsidn;
    }

    public void setSubscriberMsidn(String subscriberMsidn) {
        this.subscriberMsidn = subscriberMsidn;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTokenString() {
        return tokenString;
    }

    public void setTokenString(String tokenString) {
        this.tokenString = tokenString;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

}