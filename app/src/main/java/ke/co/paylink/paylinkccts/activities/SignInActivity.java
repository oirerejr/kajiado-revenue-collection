package ke.co.paylink.paylinkccts.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.R;
import ke.co.paylink.paylinkccts.databinding.ActivitySignInBinding;
import ke.co.paylink.paylinkccts.interfaces.ISignIn;
import ke.co.paylink.paylinkccts.models.User;
import ke.co.paylink.paylinkccts.retrofit.RetrofitInterface;
import ke.co.paylink.paylinkccts.retrofit.RetrofitSetup;
import ke.co.paylink.paylinkccts.retrofit.models.LoginResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends AppCompatActivity implements ISignIn {

    ActivitySignInBinding binding;
    private ObservableBoolean errorState = new ObservableBoolean(false);
    private ObservableBoolean isSigningIn = new ObservableBoolean(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        binding.setListener(this);
        binding.setErrorState(errorState);
        binding.setIsSigningIn(isSigningIn);
    }

    @Override
    public void OnSignIn() {

        String code = binding.code.getText().toString().trim();
        String pin = binding.pin.getText().toString().trim();

        if (TextUtils.isEmpty(code) || TextUtils.isEmpty(pin)) {
            Toast.makeText(this, "Please provide all details", Toast.LENGTH_LONG).show();
        } else {
            isSigningIn.set(true);
            errorState.set(false);
            binding.setStatusMessage("");
            binding.code.setFocusable(false);
            binding.pin.setFocusable(false);

            Intent intent = getIntent();
//            Location location = intent.getParcelableExtra("location");
            final String device = intent.getStringExtra("device");

            JsonObject loginRequest = new JsonObject();
            loginRequest.addProperty("username", code);
            loginRequest.addProperty("password", pin);
//            loginRequest.addProperty("device", device);
//            loginRequest.addProperty("longitude", String.valueOf(-122.0840104));
//            loginRequest.addProperty("latitude", String.valueOf(37.4220159));

            Call<LoginResponse> call = RetrofitSetup.createClass(RetrofitInterface.class, device)
                    .getUser(loginRequest);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    isSigningIn.set(false);
                    binding.code.setFocusable(true);
                    binding.pin.setFocusable(true);

                    LoginResponse loginResponse = response.body();
                    if (loginResponse != null) {

                        if (!loginResponse.getStatus().equals("200")) {
                            errorState.set(true);
                            binding.setStatusMessage(loginResponse.getMessage());
                        } else {
                            errorState.set(false);
                            PLSharedPrefs.get().setUser(loginResponse);
                            PLSharedPrefs.get().setTerminal(device);
                            startActivity(new Intent(SignInActivity.this, AmountActivity.class));
                            finish();
                        }

                    } else {
                        errorState.set(true);
                        binding.setStatusMessage("Unauthorized access");
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    isSigningIn.set(false);
                    errorState.set(true);
                    binding.setStatusMessage("Please try again!");
                    binding.code.setFocusable(true);
                    binding.pin.setFocusable(true);
                }
            });

        }

    }
}
