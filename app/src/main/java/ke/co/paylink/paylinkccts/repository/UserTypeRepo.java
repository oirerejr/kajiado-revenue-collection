package ke.co.paylink.paylinkccts.repository;

import java.util.Arrays;
import java.util.List;

import ke.co.paylink.paylinkccts.models.UserType;

public class UserTypeRepo {

    private static UserTypeRepo userTypeRepo;
    private final UserType agent = new UserType("Agent", "Enable transactions including \n" +
            "Card registration \n" +
            "Making payments \n" +
            "Debit/Credit Transactions \n");
    private final UserType merchant = new UserType("Merchant", "Limited to making payments for different services");

    private List<UserType> userTypes = Arrays.asList(agent, merchant);

    public static synchronized UserTypeRepo instance() {
        if (userTypeRepo == null) {
            userTypeRepo = new UserTypeRepo();
        }
        return userTypeRepo;
    }

    public List<UserType> getUserTypes() {
        return userTypes;
    }

}
