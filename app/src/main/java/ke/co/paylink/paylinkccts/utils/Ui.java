package ke.co.paylink.paylinkccts.utils;

import android.view.View;
import android.view.Window;

public class Ui {

    private static Ui ui;

    public static synchronized Ui get() {
        if (ui == null) {
            ui = new Ui();
        }
        return ui;
    }

    public void hideStatusBar(Window window) {
        int status = View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
        window.getDecorView().setSystemUiVisibility(status);
    }

}
