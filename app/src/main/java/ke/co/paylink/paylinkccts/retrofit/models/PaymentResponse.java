package ke.co.paylink.paylinkccts.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentResponse {

    @SerializedName("subscriber_msidn")
    @Expose
    private String subscriberMsidn;
    @SerializedName("card_number")
    @Expose
    private String cardNumber;
    @SerializedName("reference_number")
    @Expose
    private String referenceNumber;
    @SerializedName("merchant_pos")
    @Expose
    private String merchantPos;
    @SerializedName("token_string")
    @Expose
    private String tokenString;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    public String getSubscriberMsidn() {
        return subscriberMsidn;
    }

    public void setSubscriberMsidn(String subscriberMsidn) {
        this.subscriberMsidn = subscriberMsidn;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getMerchantPos() {
        return merchantPos;
    }

    public void setMerchantPos(String merchantPos) {
        this.merchantPos = merchantPos;
    }

    public String getTokenString() {
        return tokenString;
    }

    public void setTokenString(String tokenString) {
        this.tokenString = tokenString;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}