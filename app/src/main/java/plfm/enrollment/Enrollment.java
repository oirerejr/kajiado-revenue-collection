package plfm.enrollment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.digitalpersona.uareu.Reader.Priority;

import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.activities.AgentActivity;
import ke.co.paylink.paylinkccts.fp.Globals;
import utils.DBManager;

public class Enrollment {

    //    private String deviceName = PLSharedPrefs.get().getFPDevice();
    private String mEnginError;
    private Reader mReader = null;
    private int mDPI = 0;
    private Bitmap mBitmap = null;
    private boolean mReset = false;

    private String mTextConclusionString;
    private Engine mEngine = null;
    private int mCurrentFmdsCount = 0;
    private String mTextString;
    private boolean mFirst = true;
    private boolean mSuccess = false;
    private Fmd mEnrollmentFmd = null;
    private int mTemplateSize = 0;
    Engine.EnrollmentCallback enrollThread = null;
    private Reader.CaptureResult capResult = null;

    public interface IEnrollment {
        void onEnroll(byte[] data);

        void onTerminate(String error);

        void onText(String textConclusion, String mTextString);

        void onStep(String step);
    }

    private IEnrollment iEnrollment;
    private static Enrollment enrollment;

    public static synchronized Enrollment get(Context context, String deviceName, IEnrollment iEnrollment) {
        if (enrollment == null) {
            enrollment = new Enrollment(context, deviceName, iEnrollment);
        }
        return enrollment;
    }

    private Enrollment(Context context, String deviceName, final IEnrollment iEnrollment) {
        this.iEnrollment = iEnrollment;

        // Initialise dp sdk
        try {
            mReader = Globals.getInstance().getReader(deviceName, context);

            mReader.Open(Priority.EXCLUSIVE);
            mDPI = Globals.GetFirstDPI(mReader);
            mEngine = UareUGlobal.GetEngine();
        } catch (Exception e) {
            iEnrollment.onTerminate(e.toString());
//            PLSharedPrefs.get().setFPDevice("");
            uninitialize();
            return;
        }

        CaptureThread captureThread = null;
        captureThread = new CaptureThread(iEnrollment);
        captureThread.start();

//        // loop capture on a separate thread to avoid freezing the UI
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    mCurrentFmdsCount = 0;
//                    mReset = false;
//                    enrollThread = new EnrollmentCallback(mReader, mEngine);
//                    while (!mReset) {
//                        try {
//                            mEnrollmentFmd = mEngine.CreateEnrollmentFmd(Fmd.Format.ANSI_378_2004, enrollThread);
//                            if (mSuccess = (mEnrollmentFmd != null)) {
//                                mTemplateSize = mEnrollmentFmd.getData().length;
//                                mCurrentFmdsCount = 0;    // reset count on success
//                                iEnrollment.onEnroll(mEnrollmentFmd.getData());
////                                PLSharedPrefs.get()
//                            }
//                        } catch (Exception e) {
//                            // template creation failed, reset count
//                            mCurrentFmdsCount = 0;
//                        }
//                    }
//                } catch (Exception e) {
//                    if (!mReset) {
////                        Enrollment.this.deviceName = "";
//                        uninitialize();
//                    }
//                }
//            }
//        }).start();

    }

    private void uninitialize() {
        try {
            mReset = true;
            try {
                mReader.CancelCapture();
            } catch (Exception e) {
            }
            mReader.Close();
        } catch (Exception e) {
            Log.w("UareUSampleJava", "error during Reader shutdown");
        }
        enrollment = null;
    }

    public class EnrollmentCallback
            extends Thread
            implements Engine.EnrollmentCallback {
        public int m_current_index = 0;

        private Reader m_reader = null;
        private Engine m_engine = null;

        public EnrollmentCallback(Reader reader, Engine engine) {
            m_reader = reader;
            m_engine = engine;
        }

        // callback function is called by dp sdk to retrieve fmds until a null is returned
        @Override
        public Engine.PreEnrollmentFmd GetFmd(Fmd.Format format) {
            Engine.PreEnrollmentFmd result = null;
            while (!mReset) {
                try {
                    capResult = m_reader.Capture(Fid.Format.ANSI_381_2004, Globals.DefaultImageProcessing, mDPI, -1);
                } catch (Exception e) {
//                    deviceName = "";
                    uninitialize();
                }

                // an error occurred
                if (capResult == null || capResult.image == null) continue;

                try {
                    mEnginError = "";
                    // save bitmap image locally
                    mBitmap = Globals.GetBitmapFromRaw(capResult.image.getViews()[0].getImageData(), capResult.image.getViews()[0].getWidth(), capResult.image.getViews()[0].getHeight());
                    Engine.PreEnrollmentFmd prefmd = new Engine.PreEnrollmentFmd();
                    prefmd.fmd = m_engine.CreateFmd(capResult.image, Fmd.Format.ANSI_378_2004);
                    prefmd.view_index = 0;
                    mCurrentFmdsCount++;

                    result = prefmd;
                    break;
                } catch (Exception e) {
                    mEnginError = e.toString();
                    iEnrollment.onTerminate(mEnginError);
                }
            }

            mTextConclusionString = Globals.QualityToString(capResult);

            if (!mEnginError.isEmpty()) {
                mTextConclusionString = "Engine: " + mEnginError;
            }

            if (mEnrollmentFmd != null || mCurrentFmdsCount == 0) {
                if (!mFirst) {
                    if (mTextConclusionString.length() == 0) {
                        mTextConclusionString = mSuccess ? "Enrollment template created, size: " + mTemplateSize : "Enrollment template failed. Please try again";
                    }
                }
                mTextString = "Place any finger on the Reader";
                mEnrollmentFmd = null;
            } else {
                mFirst = false;
                mSuccess = false;
                mTextString = "Continue to place the same finger on the Reader";
            }

            iEnrollment.onText(mTextConclusionString, mTextString);

            return result;
        }
    }

    private final int CAPTURE = 0;
    private final int COMPARE = 1;
    private boolean m_bThreadFinished = false;
    boolean mCompare_bThreadFinish = false;

    StringBuffer sb;

    private DBManager mDBManager = null;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private byte[] bytesCapture;

    public class CaptureThread extends Thread {

        public boolean isThreadFinished() {
            return m_bThreadFinished;
        }

        Enrollment.IEnrollment iEnrollment;

        public CaptureThread(Enrollment.IEnrollment iEnrollment) {
            this.iEnrollment = iEnrollment;
        }

        public void run() {
            super.run();
            synchronized (this) {
                m_bThreadFinished = false;

                try {
                    while (!mReset) {

                        /*
                         *Capture Finger Print 采集特征
                         *可根据传递的值，获取不同标准的指纹模板
                         *Different standard fingerprint templates can be obtained based on the passed values
                         */
                        capResult = mReader.Capture(Fid.Format.ISO_19794_4_2005, Globals.DefaultImageProcessing, mDPI, -1);
                        Engine m_engine = UareUGlobal.GetEngine();
                        Fmd m_fmd = m_engine.CreateFmd(capResult.image, Fmd.Format.ISO_19794_2_2005);
                        bytesCapture = m_fmd.getData();
                        if (bytesCapture.length > 0) {
                            iEnrollment.onEnroll(bytesCapture);
                            uninitialize();
                        }

                        Log.e("liuhao", "*******************CAPTURE******************");

                        /******************************************************/
                        /***********add by liuhao 20180522 START***************/
                        /*
                         *When you save the retrieved fingerprint ISO / ANSI bytes for database storage, you need to
                         *convert them to the basic format supported by the database. We recommend that you always  *convert them to Base64
                         *将获取到的指纹ISO / ANSI格式bytes进行数据库保存时，需要转换成数据库支持的基本格式，我们建议一定要转换成Base64
                         */

                        //Store to database 存储到数据库

//                    setValue(FID);
                        /***********add by liuhao 20180522 END***************/
                        /******************************************************/
//                   /*
//                    Use Globals.GetBitmapFromRaw from raw to bitmap
//                    通过Globals.GetBitmapFromRaw 转换成bitmap
//                    */
//                    m_bitmap = Globals.GetBitmapFromRaw(cap_result.image.getViews()[0].getImageData(), cap_result.image.getViews()[0].getWidth(), cap_result.image.getViews()[0].getHeight());
//                    m_text_conclusionString = Globals.QualityToString(cap_result);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            binding.scanButton.setText(m_text_conclusionString);
//                        }
//                    });
                    }
                } catch (final UareUException e) {
                    if (!mReset) {
                        iEnrollment.onTerminate(e.toString());
//                        Log.e("UareUSampleJava", "error during capture: " + e.toString());
                        uninitialize();
                    }
                }
                m_bThreadFinished = true;
            }
        }
    }


}
