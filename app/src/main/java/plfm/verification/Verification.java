package plfm.verification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.UareUGlobal;

import java.text.DecimalFormat;

import ke.co.paylink.paylinkccts.PLSharedPrefs;
import ke.co.paylink.paylinkccts.fp.Globals;
import plfm.enrollment.Enrollment;

public class Verification {

    private String m_deviceName = "";

    private String m_enginError;

    private Reader m_reader = null;
    private int m_DPI = 0;
    private Bitmap m_bitmap = null;
    private boolean m_reset = false;

    private String m_textString;
    private String m_text_conclusionString;
    private Engine m_engine = null;
    private Fmd m_fmd = null;
    private int m_score = -1;
    private boolean m_first = true;
    private boolean m_resultAvailableToDisplay = false;
    private Reader.CaptureResult cap_result = null;

    public interface IVerification {
        void onVerify(byte[] data);

        void onTerminate(String error);

        void onMessage(String message);
    }

    private IVerification iVerification;
    private static Verification verification;

    public static synchronized Verification get(Context context, String deviceName, IVerification iVerification) {
        if (verification == null) {
            verification = new Verification(context, deviceName, iVerification);
        }
        return verification;
    }

    private Verification(Context context, String deviceName, final IVerification iVerification) {
        this.iVerification = iVerification;

        // initiliaze dp sdk
        try {
            Context applContext = context.getApplicationContext();
            m_reader = Globals.getInstance().getReader(m_deviceName, applContext);
            m_reader.Open(Reader.Priority.EXCLUSIVE);
            m_DPI = Globals.GetFirstDPI(m_reader);
            m_engine = UareUGlobal.GetEngine();
        } catch (Exception e) {
            Log.w("UareUSampleJava", "error during init of reader");
            m_deviceName = "";
            uninitialize();
            return;
        }

        // loop capture on a separate thread to avoid freezing the UI
        new Thread(new Runnable() {
            @Override
            public void run() {
                m_reset = false;
                while (!m_reset) {
                    try {
                        cap_result = m_reader.Capture(Fid.Format.ANSI_381_2004, Globals.DefaultImageProcessing, m_DPI, -1);
                    } catch (Exception e) {
                        if (!m_reset) {
                            m_deviceName = "";
                            uninitialize();
                        }
                    }

                    m_resultAvailableToDisplay = false;

                    // an error occurred
                    if (cap_result == null || cap_result.image == null) continue;

                    try {
                        m_enginError = "";

                        // save bitmap image locally
//                        m_bitmap = Globals.GetBitmapFromRaw(cap_result.image.getViews()[0].getImageData(), cap_result.image.getViews()[0].getWidth(), cap_result.image.getViews()[0].getHeight());

                        if (m_fmd == null) {
                            m_fmd = m_engine.CreateFmd(cap_result.image, Fmd.Format.ANSI_378_2004);
                            iVerification.onVerify(m_fmd.getData());
                            m_fmd = null;
                        }
//                        else {
//                            m_score = m_engine.Compare(m_fmd, 0, m_engine.CreateFmd(cap_result.image, Fmd.Format.ANSI_378_2004), 0);
//                            m_fmd = null;
//                            m_resultAvailableToDisplay = true;
//                        }
                    } catch (Exception e) {
                        m_enginError = e.toString();
                        Log.w("UareUSampleJava", "Engine error: " + e.toString());
                    }

//                    m_text_conclusionString = Globals.QualityToString(cap_result);
//                    if (!m_enginError.isEmpty()) {
//                        m_text_conclusionString = "Engine: " + m_enginError;
//                    } else if (m_fmd == null) {
//                        if ((!m_first) && (m_resultAvailableToDisplay)) {
//                            if (m_text_conclusionString.length() == 0) {
//                                DecimalFormat formatting = new DecimalFormat("##.######");
//                                m_text_conclusionString = "Dissimilarity Score: " + String.valueOf(m_score) + ", False match rate: " + Double.valueOf(formatting.format((double) m_score / 0x7FFFFFFF)) + " (" + (m_score < (0x7FFFFFFF / 100000) ? "match" : "no match") + ")";
//                            }
//                        }
//
//                        m_textString = "Place any finger on the reader";
//                    } else {
//                        m_first = false;
//                        m_textString = "Place the same or a different finger on the reader";
//                    }

//                    iVerification.onMessage(m_textString);

                }
            }
        }).start();

    }

    private void uninitialize() {
        try {
            m_reset = true;
            try {
                m_reader.CancelCapture();
            } catch (Exception e) {
            }
            m_reader.Close();

        } catch (Exception e) {
            Log.w("UareUSampleJava", "error during reader shutdown");
        }
    }

}